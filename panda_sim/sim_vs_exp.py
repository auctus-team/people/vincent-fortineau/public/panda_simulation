# %% IMPORTS 
# %matplotlib qt

import numpy as np
import math
import time

import pinocchio as pin
from matplotlib import pyplot as plt

from tqdm import tqdm # for progression bar

from panda_sim.utils.robot_solver import RobotSolver
from panda_sim.utils.task_ctrl import TaskController
from panda_sim.simulation import RK4, robot_state_ode, computeCmd

# %% MACROS
# simulation
BASE = 'world'
TIP_LINK = 'panda_link8'
LOOP_FREQUENCY = 1e3 # Hz
# MAIN TASK GAINS
KP = np.array([1000,1000,1000,63,63,63])
KD = 2*np.sqrt(KP)
# SECONDARY TASK GAINS
KQP = np.array([100,100,100,100,100,100,100])
KQD = 2*np.sqrt(KQP)
# PHI FRICTION TORQUE COEFFICIENTS (values taken from Gaz et al (2019) supplementary data)
PHI1 = np.array([0.54615, 8722.4, 0.64068, 1.2794, 0.83904, 0.30301, 0.56489]) # N.m
PHI2 = np.array([5.1181, 9.0657, 10.136, 5.5903, 8.3469, 17.133, 10.336]) # s/rad
PHI3 = np.array([3.9533e-02, 2.5882e-02, -4.6070e-02, 3.6194e-02, 2.6226e-02, -2.1047e-02, 3.5526e-03]) # rad/s
# FV,FC&FO FRICTION TORQUE COEFFICIENTS (values taken from Gaz et al (2019) supplementary data)
FV = np.array([0.0665, 0.1987, 0.0399, 0.2257, 0.1023, -0.0132, 0.0638])
FC = np.array([0.2450, 0.1523, 0.1827, 0.3591, 0.2669, 0.1658, 0.2109])
FO = np.array([-0.1073, -0.1566, -0.0686, -0.2522, 0.0045, 0.0910, -0.0127])
#
USE_PHI = 1 
USE_F = 2
FRIC_METH = USE_F = 2 # set to any other value to avoid friction modelling
# experimental
# should be in: panda_sim/data/ 
FILE_NAME = "config_1/test_10N_x.csv"
# FILE_NAME = "test_real.cs,v" 
# FILE_NAME = "test_real_all_axis.csv" 
# FILE_NAME = "test_real_all_axis_config_2.csv" 
# FILE_NAME = "test_real_all_axis_config_singular.csv" 
MULTIPLE_FILES = False
FILE_LIST = ['test_10N_x', 'test_-10N_x', 'test_10N_y', 'test_-10N_y', 'test_10N_z', 'test_-10N_z']
FILE_LIST_FOLDER = 'config_1/'
FILE_LIST_EXTENSION = '.csv'
FILE_LIST = [FILE_LIST_FOLDER + file_name + FILE_LIST_EXTENSION for file_name in FILE_LIST]
ARM_ID = 'panda_1'
NB_JOINTS = 7
STEP = 0.001 # 1 ms

# %% CLASSES

class RobotState:
   
  def __init__(self, nb_joints:int, t:(np.array or None)=None, q=None, dq=None, ddq=None, tau=None, fext=None):
   
    self.nq = nb_joints # dummy_rob.model.nq # number of joints

    if t is not None:
      self.ns = t.size # number of samples
      self.t = np.array(t).reshape((self.ns,))
    else:
      self.ns = 0
      self.t = np.zeros((self.ns,))

    if q is not None:
      self.q = np.array(q).reshape((self.nq, self.ns))
    else:
      self.q = np.zeros((self.nq, self.ns))
    
    if dq is not None:
      self.dq = np.array(dq).reshape((self.nq, self.ns))
    else:
      self.dq = np.zeros((self.nq, self.ns))
    if ddq is not None:
      self.ddq = np.array(ddq).reshape((self.nq, self.ns))
    else:
      self.ddq = np.zeros((self.nq, self.ns))
    if tau is not None:
      self.tau = np.array(tau).reshape((self.nq, self.ns))
    else:
      self.tau = np.zeros((self.nq, self.ns))
    if fext is not None:
      self.fext = np.array(fext).reshape((6, self.ns))
    else:
      self.fext = np.zeros((6, self.ns)) # external forces 
    
    # self.t_max = dummy_rob.t_max
    # self.t_min = dummy_rob.t_min
    # # maximal joint velocities
    # self.dq_max = dummy_rob.dq_max
    # self.dq_min = dummy_rob.dq_min
    # # maximal joint angles
    # self.q_max = dummy_rob.q_max
    # self.q_min = dummy_rob.q_min 
    # # max joint acc
    # self.ddq_max = dummy_rob.ddq_max   
    # self.ddq_min = -dummy_rob.ddq_min
    # # max joint jerk
    # self.dddq_max = dummy_rob.dddq_max      
    # self.dddq_min = dummy_rob.dddq_min

  def set_joint_states(self, t=None, q=None, dq=None, ddq=None, tau=None, fext=None):
    if t is not None:
      self.t = np.array(t).reshape((self.ns,))
    if q is not None:
      self.q = np.array(q).reshape((self.nq, self.ns))
    if dq is not None:
      self.dq = np.array(dq).reshape((self.nq, self.ns))
    if ddq is not None:
      self.ddq = np.array(ddq).reshape((self.nq, self.ns))
    if tau is not None:
      self.tau = np.array(tau).reshape((self.nq, self.ns))
    if fext is not None:
      self.fext = np.array(fext).reshape((6, self.ns))
  
  # add a single state
  def add_joint_states(self, t, q, dq=None, ddq=None, tau=None, fext=None):
    
    self.ns = self.ns + 1
    self.t = np.append(self.t, t)
    self.q = np.concatenate((self.q, np.array(q).reshape((self.nq, 1))), axis=1)

    nan_arr = np.empty((self.nq, 1))
    nan_arr[:] = np.nan

    if dq is not None:
      self.dq = np.concatenate((self.dq, np.array(dq).reshape((self.nq, 1))), axis=1)
    else:
      self.dq = np.concatenate((self.dq, nan_arr), axis=1)

    if ddq is not None:
      self.ddq = np.concatenate((self.ddq, np.array(ddq).reshape((self.nq, 1))), axis=1)
    else:
      self.ddq = np.concatenate((self.ddq, nan_arr), axis=1)

    if tau is not None:
      self.tau = np.concatenate((self.tau, np.array(tau).reshape((self.nq, 1))), axis=1)
    else:
      self.tau = np.concatenate((self.tau, nan_arr), axis=1)

    if fext is not None:
      self.fext = np.concatenate((self.fext, np.array(fext).reshape((6, 1))), axis=1)
    else:
      nan_arr = np.empty((6, 1))
      nan_arr[:] = np.nan
      self.fext = np.concatenate((self.fext, nan_arr), axis=1)
    
  
# %% FUNCTIONS

# https://stackoverflow.com/a/6520696
def nan_helper(y):
  """Helper to handle indices and logical indices of NaNs.

  Input:
      - y, 1d numpy array with possible NaNs
  Output:
      - nans, logical indices of NaNs
      - index, a function, with signature indices= index(logical_indices),
        to convert logical indices of NaNs to 'equivalent' indices
  Example:
      >>> # linear interpolation of NaNs
      >>> nans, x= nan_helper(y)
      >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
  """

  return np.isnan(y), lambda z: z.nonzero()[0]

#
def interpNaN(y):
  nans, x = nan_helper(y)
  y[nans] = np.interp(x(nans), x(~nans), y[~nans])
  return y

#
def read_raw_csv(file_name:str):
  file_path = "data/" + file_name
  return np.genfromtxt(file_path, delimiter=',', names=True, deletechars='')

#
def extract_data(raw_data, arm_id, nb_joints, set_t0=False):
  '''
  Returns a numpy array with the time, the joint poses, the joint efforts and the wrenches.
  The function also returns an array with the indexes to easily acces the previous elements
  idxs[0] returns 0, the time index
  idxs[1] returns [1,2,3,4,5,6,7], the joint poses indexes
  idxs[2] returns [8,9,10,11,12,13,14], the joint efforts indexes ...
  '''
  # topic names for robot state
  axes = ['x', 'y', 'z']
  wrenches = ['force','force','force','torque','torque','torque']
  # simulated external forces injected at torque command (tau = tau_c - J^t f_ext) 
  #wrenches_global_name = '/'+arm_id+'/franka_state_controller/F_ext/wrench/' # external forces from the environment reference, in the endpoint frame
  wrenches_global_name = '/external_force/wrench/' #
  franka_state_global_name = '/'+arm_id+'/franka_state_controller/joint_states/'+arm_id+'_joint'

  t = raw_data['__time']
  t.shape = [1, len(t)]
  if set_t0:
    t = t - t[0,0]

  joint_data = np.array([interpNaN(raw_data[franka_state_global_name+str(i+1)+'/position']) for i in range(0, nb_joints)])
  eff_data = np.array([interpNaN(raw_data[franka_state_global_name+str(i+1)+'/effort']) for i in range(0, nb_joints)])
  wrench_data = np.array([interpNaN(raw_data[wrenches_global_name+wrenches[i]+'/'+axes[i%3]]) for i in range(0, 6)]) # 6 DOF

  # indexes to acces time, joint pose, joint eff and wrench data
  idxs = []
  idxs.append(0) # time idx
  idxs.append(list(range(1, nb_joints+1))) # joint_pose_idx
  idxs.append(list(range(nb_joints+1, 2*nb_joints+1))) # joint_eff_idx
  idxs.append(list(range(2*nb_joints+1, 2*nb_joints+6+1))) # wrench_idx

  return np.concatenate((t, joint_data, eff_data, wrench_data), axis=0), idxs

#
def interpolate_data(data, t_index, step, t0=None, tend=None):
  t = data[t_index]
  if t0 is None:
    t0 = t[0]
  if tend is None:
    tend = t[-1]
  t_new = np.arange(t0, tend, step)
  #
  new_data = np.array([np.interp(t_new, t, data[i]) for i in range(0, len(data)) if i != t_index])
  t_new.shape = [1, len(t_new)] 
  new_data = np.concatenate((t_new, new_data)) 
  #
  return new_data

#
def plot_joint_position(exp_data, sim_data):
  fig, axs = plt.subplots(2,4)
  for it in range(0,7):
    axs[math.floor(it/4),it%4].plot(exp_data.t, exp_data.q[it,:], linewidth=1.5, color='r', alpha=0.8, label='Experimental')
    axs[math.floor(it/4),it%4].plot(exp_data.t, sim_data.q[it,:], linewidth=0.8, color='b', alpha=0.8, label='Simulation')
  axs[-1,-2].legend()
  axs[-1,-1].set_visible(False)
  fig.suptitle('Joint position data (m)')

#
def plot_joint_effort(exp_data, sim_data):
  fig, axs = plt.subplots(2,4)
  for it in range(0,7):
    axs[math.floor(it/4),it%4].plot(exp_data.t, exp_data.tau[it,:], linewidth=1.5, color='r', alpha=0.8, label='Experimental')
    axs[math.floor(it/4),it%4].plot(exp_data.t, sim_data.tau[it,:], linewidth=0.8, color='b', alpha=0.8, label='Simulation')
  axs[-1,-2].legend()
  axs[-1,-1].set_visible(False)
  fig.suptitle('Joint effort data (Nm)')

#
def plot_endpoint_position(exp_data, sim_data, title=None):
  exp_x = [pin.SE3(robot.forward(q)) for q in exp_data.q.T]
  sim_x = [pin.SE3(robot.forward(q)) for q in sim_data.q.T]

  exp_position = np.array([pose.translation for pose in exp_x])
  sim_position = np.array([pose.translation for pose in sim_x])

  axis = ['X', 'Y', 'Z']

  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    axs[it].plot(exp_data.t, exp_position[:,it], linewidth=2, color='r', alpha=0.8, label='Experimental')
    axs[it].plot(exp_data.t, sim_position[:,it], linewidth=1.5, color='b', alpha=0.8, label='Simulation')
    axs[it].title.set_text(axis[it] + ' axis')
    if any(exp_data.fext[it,:]):
      ax_f = axs[it].twinx()
      ax_f.plot(exp_data.t, exp_data.fext[it,:], linewidth=1.5, color='k', linestyle='dashed', alpha=0.8, label='Force Reaction')
  axs[-1].legend()
  ax_f.legend()
  if title is None:
    fig.suptitle('Endpoint position')
  else:
    fig.suptitle(title)

def delete_string_sequences(main_string, string_sequences):
  for sequence in string_sequences:
    main_string = main_string.replace(sequence, '')
  return main_string

#######################
# %% MAIN
if __name__ == "__main__":
  robot = RobotSolver(BASE, TIP_LINK, is_visualisation=False)
  if FRIC_METH == USE_PHI:
    robot.set_phi_friction_gains(PHI1, PHI2, PHI3)
  elif FRIC_METH == USE_F:
    robot.set_coulomb_friction_gains(FC, FO)
    robot.set_viscous_friction_gain(FV)

  # set up controllers gains and targets
  main_task = TaskController(ctrl_mode='endpoint', damping_gain=KD, stiffness_gain=KP)
  secondary_task = TaskController(ctrl_mode='joint', nb_joint_ctrl=robot.model.nq, damping_gain=KQP, stiffness_gain=KQD)

  if not MULTIPLE_FILES:
    ###########################
    # collect experimental data
    ###########################
    data, idxs = extract_data(read_raw_csv(FILE_NAME), ARM_ID, NB_JOINTS, set_t0=True)
    data = interpolate_data(data, 0, step=STEP) # interpolation at 1ms

    t_idx = 0
    joint_pose_idx = list(range(1, NB_JOINTS+1))
    joint_eff_idx = list(range(NB_JOINTS+1, 2*NB_JOINTS+1))
    wrench_idx = list(range(2*NB_JOINTS+1, 2*NB_JOINTS+6+1)) # 6 DOF

    exp_data = RobotState(NB_JOINTS, t=data[t_idx], q=data[joint_pose_idx], tau=data[joint_eff_idx], fext=data[wrench_idx])
    del data

    #################################
    # simulate the experimental data
    #################################
    robot.update_joint_data(q=exp_data.q[:,0])
    sim_data = RobotState(nb_joints=7)  
    tau = exp_data.tau[:,0]

    main_task.setEndpointTarget(x_star=pin.SE3(robot.forward())) # update marker to current pose
    secondary_task.setJointTarget(q_star=robot.q) # update to current joint position

    start_time = time.time()
    for t in tqdm(exp_data.t, desc="Simulation  in progress..", miniters=10):
      idx = np.where(exp_data.t==t)[0].squeeze()
      #
      sim_data.add_joint_states(t, q=robot.q, dq=robot.dq, tau=tau, fext=exp_data.fext[:,idx])
      #
      tau = computeCmd(robot, main_task, secondary_task, is_saturation=False)
      dstate = RK4(robot_state_ode, np.concatenate((robot.q, robot.dq)), np.concatenate((tau, exp_data.fext[:,idx])), STEP, arg=robot)
      robot.update_joint_data(dstate[0:robot.model.nq], dstate[robot.model.nq:], apply_saturation=False)

    #######################################
    # compare experimental and simulated joint data
    #######################################
    plot_joint_position(exp_data, sim_data)
    plot_joint_effort(exp_data, sim_data)

    #######################################
    # compute endpoint position data
    #######################################
    plot_endpoint_position(exp_data, sim_data)

  # %% Multiple files case
  if MULTIPLE_FILES:
    exp_data_list = []
    sim_data_list = []
    t_idx = 0
    joint_pose_idx = list(range(1, NB_JOINTS+1))
    joint_eff_idx = list(range(NB_JOINTS+1, 2*NB_JOINTS+1))
    wrench_idx = list(range(2*NB_JOINTS+1, 2*NB_JOINTS+6+1)) # 6 DOF

    for file_idx, file_name in enumerate(FILE_LIST):
      #############################
      # collect experimental data
      #############################
      print("Reading file: {}...\n".format(file_name))
      data, idxs = extract_data(read_raw_csv(file_name), ARM_ID, NB_JOINTS, set_t0=True)
      data = interpolate_data(data, 0, step=STEP) # interpolation at 1ms
      exp_data = RobotState(NB_JOINTS, t=data[t_idx], q=data[joint_pose_idx], tau=data[joint_eff_idx], fext=data[wrench_idx])
      del data

      #############################
      # simulate the experimental data
      #############################
      robot.update_joint_data(q=exp_data.q[:,0]) # set intial joint config
      #
      main_task.setEndpointTarget(x_star=pin.SE3(robot.forward())) # update marker to current pose
      secondary_task.setJointTarget(q_star=robot.q) # update to current joint position
      #
      sim_data = RobotState(nb_joints=7)  
      tau = exp_data.tau[:,0]
      #
      start_time = time.time()
      for t in tqdm(exp_data.t, desc="Simulation n{} in progress..".format(file_idx), miniters=10):
        idx = np.where(exp_data.t==t)[0].squeeze()
        #
        sim_data.add_joint_states(t, q=robot.q, dq=robot.dq, tau=tau, fext=exp_data.fext[:,idx])
        #
        tau = computeCmd(robot, main_task, secondary_task, is_saturation=False)
        dstate = RK4(robot_state_ode, np.concatenate((robot.q, robot.dq)), np.concatenate((tau, exp_data.fext[:,idx])), STEP, arg=robot)
        robot.update_joint_data(dstate[0:robot.model.nq], dstate[robot.model.nq:], apply_saturation=False)

      print("Duration of the simulation n{}: {:.1f}s".format(file_idx, time.time() - start_time))  
      
      # 
      exp_data_list.append(exp_data)
      sim_data_list.append(sim_data)

      plot_endpoint_position(exp_data, sim_data, title=delete_string_sequences(file_name, [FILE_LIST_FOLDER, FILE_LIST_EXTENSION]))
      plt.pause(2)

      del exp_data, sim_data
# %%
