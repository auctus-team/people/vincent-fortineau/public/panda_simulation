import numpy as np

# URDF parsing an kinematics 
import pinocchio as pin
import scipy.signal as sig
from scipy.integrate import odeint
import meshcat
# import control as ctrl
from typing import Callable

from panda_sim.utils.robot_solver import RobotSolver
import pycapacity.algorithms as alg
import pycapacity.robot as rob

class RobotStaticErrorSolver:
    
  def __init__(self, robot_solver:RobotSolver, ctrl_method):        
      self.robot = robot_solver
      self.control_type = ctrl_method

  def computeSEMatrix(self, q:np.ndarray, KP:'np.ndarray[np.ndarray]'):
     if self.control_type == 'endpoint_impedance':
        return self.computeSEMatrixFromEndpointPDController(q, KP, True)

  def computeSEMatrixFromEndpointPDController(self, q:np.ndarray, KP:'np.ndarray[np.ndarray]', _6D: bool=False) -> 'np.ndarray[np.ndarray]':
    """
    Computation of static endpoint error polytope for a torque command trying to achieve a given endpoint acceleration (with dynamic model compensation) when subject to unmodeled external force

      .. math:: \ddot{x}_d = \ddot{x}^* + K_D(\dot{x}^* - \dot{x}) + K_P(x^* - x)
      .. math:: \tau_c = A(q)J(q)^+(\ddot{x}_d - \dot{J}(q)\dot{q}) + h(\dot{q}, q)  
      .. math:: \ddot{\epsilon}^* = \ddot{x}^* - \ddot{x} = -K_D\dot{\epsilon}^* - K_P\epsilon^* + J(q) A(q)^{-1} J(q)^t f_{ext}
      .. math:: \lim_{t->\infty} \epsilon^*(t) = \lim_{s->0} s\epsilon^*(s) = -K_P^{-1} J(q_0)A(q_0)^{-1}J(q_0)^t. f(s)

    Args:
        q:      robot joint configuration (numpy.ndarray)
        KP:     proportionnal gain matrix (numpy.ndarray)
        _6D:    6D or 3D dimension (bool)
    Returns
    --------
        M(list):  endpoint static error matrix (6x6) or (3x3)

    """

    if _6D:
        if KP.shape != (6,6):
            raise ValueError("The option 6D has been choosen, the endpoint gain matrix KP should be (6x6). Here KP: {}'.format(KP.shape)")
        J = self.robot.jacobian(q)
    else:
        if KP.shape != (3,3):
            raise ValueError("The option 3D has set by default, the endpoint gain matrix KP should be (3x3). Here KP: {}'.format(KP.shape)")
        J = self.robot.jacobian(q)[:3,:]

    M = np.linalg.inv(KP).dot(J).dot(np.linalg.inv(self.robot.mass_matrix(q))).dot(J.T)

    return M
  
  def computeSE(self, q:np.ndarray, KP:'np.ndarray[np.ndarray]', f_ext:np.ndarray) -> np.ndarray:
    return self.computeSEMatrix(q, KP).dot(f_ext)
  
  def computeSEPolytopeMesh(self, q:np.ndarray, KP:'np.ndarray[np.ndarray]', f_abs:np.ndarray):
    A = self.computeSEMatrix(q, KP)
    # polytope computation
    H, d = alg.hyper_plane_shift_method(A[0:3,0:3], -f_abs*np.ones((3,1)), f_abs*np.ones((3,1))) # only forces are represented
    # vertices and faces
    vertices, faces_index = alg.hsapce_to_vertex(H, d)
    #  
    vertices = np.array(vertices).T
    # faces = rob.face_index_to_vertex(vertices, faces_index)
    # Create a Meshcat geometry object for the polytope
    # polytope = meshcat.geometry.TriangularMeshGeometry(vertices, faces_index)
    # visual = meshcat.geometry.MeshPhongMaterial(color=0xffffff, opacity=0.4)

    return vertices, faces_index
  
  

  def computeDynamicErrorSS(self, KP, KD, q0, f_in, dt=None):
    """
    Compute the non-time-varying state space model of the dynamic error for small movements around the joint configuration q0
    """
    iM_q0 = self.robot.jacobian(q0).dot(np.linalg.inv(self.robot.mass_matrix(q0))).dot(self.robot.jacobian(q0).T)
    # A = [[np.zeros((6,6)), np.eye(6)], 
    #      [      -KP      ,   -KD    ]]
    A = np.concatenate((\
        np.concatenate((np.zeros((6,6)), np.eye(6)), axis=1), \
        np.concatenate((-KP, -KD), axis=1)))
    # B = [[np.zeros((6,6))],
    #      [     iM_q0     ]]
    B = np.concatenate((np.zeros((6,6)), iM_q0))
    # C = [np.eye(6), np.zeros((6,6))]
    C = np.concatenate((np.eye(6), np.zeros((6,6))), axis=1)
    D = np.zeros((6,6))

    sys = sig.StateSpace(A, B, C, D)
    if dt is None:
       dt = 1e-3
    t = np.arange(0,np.max(f_in.shape),dt)
    tout, yout, _ = sig.lsim(sys, f_in, t)

    return tout, yout

  def computeDynamicErrorTVSS(self, KP, KD, q:np.ndarray['np.ndarray'], f_in_func:(Callable or None)=None, dt=1e-3):
    """
    Compute the time-varying state space model of the dynamic error, according to q variations, 
    using f_in_func as a input force function that will be evaluated f_in(t)
    """
    
    # if the function is not specified, it is a step function on z axis only by default
    if f_in_func is None:
      def f_in_func(t):
        if t==0:
          return np.zeros(6)
        else:
          u = np.zeros(6)
          u[2] = 1
          return u
     
    def dynamicTVErrorFromEndpointPDController(X, t, self, q_vec, dt):
      it = int(t/dt) # iteration nb to get the right q from array q_vec
      if it >= q_vec.shape[0]:
         # if the step taken by odeint is outside max t value computed...
         q = q_vec[-1,:]
      else:
        q = np.array(q_vec[it,:])
      Eps = np.array(X[0:6])
      dEps = np.array(X[6:])
      ddEps = -KP.dot(Eps) - KD.dot(dEps) + self.robot.jacobian(q).dot(np.linalg.inv(self.robot.mass_matrix(q))).dot(self.robot.jacobian(q).T).dot(f_in_func(t))
      dX = np.concatenate((dEps, ddEps))
      return dX

    t = np.arange(0,np.max(q.shape)*dt,dt) # TODO: warning the q vector must have more iterations than joints

    if self.control_type == 'endpoint_impedance':
      f = dynamicTVErrorFromEndpointPDController
    else: 
      raise Exception('Unknown control method')

    yout = odeint(f, y0=np.zeros(12), t=t, args=(self,q,dt))

    return yout