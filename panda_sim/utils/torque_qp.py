import numpy as np

import qpsolvers as qp
from panda_sim.utils.robot_solver import RobotSolver


class TorqueQP:

    def __init__(self, rob_solv: RobotSolver, dt: (float or None) = None, k_reg: (np.ndarray[float] or None) = None, rw: float = None, solver: (str or None) = None):
        #
        self.robot = rob_solv
        # self.nb_variables = self.robot.model.nv
        # self.nb_constraints = self.robot.model.nv

        if dt is not None:
            self.set_control_period(dt)

        if k_reg is not None:
            self.set_regularisation_gains(k_reg)

        if rw is not None:
            self.set_regularisation_weight(rw)

        available_solvers = qp.available_solvers
        if solver is not None:
            if solver.lower() in available_solvers:
                self.solver = solver
            else:
                print(
                    'The solver {solver} is not available. Please install it or choose an other one. To check available solvers use qpsolvers.available_solvers.')

        if (not hasattr(self, 'solver')) and len(available_solvers) > 0:
            self.solver = available_solvers[0]
            print('The solver {self.solver} will be used for the qp problem.')

    def set_control_period(self, dt: float):
        self.dt = dt

    def set_regularisation_gains(self, k_reg: (np.ndarray[float] or np.ndarray[np.ndarray[float]])):
        # check input
        if not isinstance(k_reg, np.ndarray):
            k_reg = np.array(k_reg)

        if k_reg.ndim == 1 and k_reg.size == self.robot.model.nq:
            self.K_reg = np.diag(k_reg)
        elif k_reg.ndim == 2 and (k_reg.shape[0] == 1 or k_reg.shape[1] == 1):
            self.K_reg = np.diag(k_reg.squeeze())
        elif k_reg.ndim == 2 and (k_reg.shape[0] == self.robot.model.nq and k_reg.shape[1] == self.robot.model.nq):
            self.K_reg = k_reg
        else:
            raise ValueError(
                f"k_reg must either be a 1D array of size {self.model.nq}, or a 2D matrix of size {self.model.nq}x{self.model.nq}")

    def set_regularisation_weight(self, rw: float):
        self.r_w = rw

    def update(self, q: np.ndarray[float], dq: np.ndarray[float], xdd_des: np.ndarray[float]):

        Cdq = self.robot.coriolis_matrix(q, dq).dot(dq)
        A = self.robot.mass_matrix(q)
        A_inv = np.linalg.inv(A)
        g = self.robot.gravity_torque(q)
        # J = self.robot.jacobian(q)
        dJ = self.robot.jacobian_dot(q, dq)

        a = self.robot.jacobian(q).dot(A_inv)
        # main task minimizing the quadratic norm || xdd_des - xdd ||
        hessian = 2*a.T.dot(a)
        gradient = 2*a.T.dot(dJ.dot(dq) - a.dot(Cdq + g) - xdd_des)
        # regularisation task
        hessian = hessian + 2*self.r_w*A_inv
        gradient = gradient + 2*self.r_w*A_inv.dot(self.K_reg.dot(dq))
        # torques boundaries ub < tau < lb
        # Joint velocities and position contraints are explained relatively to the torque as detailed in
        # Lucas Joseph. An energetic approach to safety in robotic manipulation. Sorbonne Université, 2018.
        # in 2.3.1 Intrinsic constraints
        lb = self.robot.t_min
        ub = self.robot.t_max

        horizon = 15*self.dt
        non_linear_terms = Cdq + g
        
        # the constraints bmin < A_t * tau < bmax are concatenated in a single upper inequality
        # (A_t, -A_t)^t * tau < (b_max, -b_min)^t
        b_ub = np.concatenate(( 
            np.minimum.reduce([(self.robot.dq_max - dq)/horizon + A_inv.dot(non_linear_terms),
            2*(self.robot.q_max - q - dq.dot(horizon))/(horizon**2) + A_inv.dot(non_linear_terms)]),
            np.minimum.reduce([-(self.robot.dq_min - dq)/horizon - A_inv.dot(non_linear_terms),
            -2*(self.robot.q_min - q - dq.dot(horizon))/(horizon**2) - A_inv.dot(non_linear_terms)]) 
        ))
            
        # This method does not seems to work
        # In this case the constraints are multiplied by the inertia matrix to be compared to tau, and not A^-1*tau
        # Only the most restrictive joint contraint is kept between joint torque, velocity and position
        # lb = np.maximum.reduce([self.robot.t_min,
        #                 A.dot(self.robot.dq_min - dq)/horizon + non_linear_terms,
        #                 2*A.dot(self.robot.q_min - q - dq.dot(horizon))/(horizon**2) + non_linear_terms])

        # ub = np.minimum.reduce([self.robot.t_max,
        #                 A.dot(self.robot.dq_max - dq)/horizon + non_linear_terms,
        #                 2*A.dot(self.robot.q_max - q - dq.dot(horizon))/(horizon**2) + non_linear_terms])

        # print("Lower bounds torque: {}".format(self.robot.t_min - g))
        # print("Lower bounds velocity: {}".format(
        #     A.dot(self.robot.dq_min - dq)/horizon + non_linear_terms))
        # print("Lower bounds position: {}".format(
        #     2*A.dot(self.robot.q_min - q - dq.dot(horizon))/(horizon**2) + non_linear_terms))
        # print("Lower bounds: {}".format(lb))
        # print("Upper bounds torque: {}".format(self.robot.t_max - g))
        # print("Upper bounds velocity: {}".format(
        #     A.dot(self.robot.dq_max - dq)/horizon + non_linear_terms))
        # print("Upper bounds position: {}".format(
        #     2*A.dot(self.robot.q_max - q - dq.dot(horizon))/(horizon**2) + non_linear_terms))
        # print("Upper bounds: {}".format(ub))

        return qp.solve_qp(P=hessian, q=gradient, G=np.vstack((A_inv,-A_inv)), h=b_ub, ub=ub, lb=lb, solver=self.solver)
