#!/usr/bin/env python3

import os
import numpy as np

# URDF parsing an kinematics 
import pinocchio as pin
from pinocchio.visualize import MeshcatVisualizer
import meshcat

class RobotSolver:

    # constructor - reading urdf and constructing the robots kinematic chain
    def __init__(self, base:str, tip:str, q:(np.array or None)=None, urdf_path:(str or None)=None, is_visualisation:bool=False, mesh_path:(str or None)=None):
        if urdf_path is None:
            path = os.path.dirname(os.path.abspath(__file__))
            urdf_path = os.path.join(path, 'panda.urdf')

        if mesh_path is None:
            mesh_path = os.path.dirname(os.path.abspath(__file__))

        # loading the root urdf from robot_description parameter
        if is_visualisation:
            self.model, self.collision_model, self.visual_model = pin.buildModelsFromUrdf(urdf_path, mesh_path)
        else:
            self.model = pin.buildModelFromUrdf(urdf_path)

        self.base_link = base
        self.tip_link = tip
        self.base_id = self.model.getFrameId(base)
        self.tip_id = self.model.getFrameId(tip)

        # maximal torques
        self.t_max = self.model.effortLimit.T
        self.t_min = -self.t_max
        # maximal joint velocities
        self.dq_max = self.model.velocityLimit.T
        self.dq_min = -self.dq_max
        # maximal joint angles
        self.q_max = self.model.upperPositionLimit.T
        self.q_min = self.model.lowerPositionLimit.T
        # https://frankaemika.github.io/docs/control_parameters.html
        # TODO: for more general use
        # max joint acc
        self.ddq_max = np.array([15,7.5,10,12.5,15,20,20]).T       
        self.ddq_min = -self.ddq_max
        # max joint jerk
        self.dddq_max = np.array([7500.0,3750.0,5000.0,6250.0,7500.0,10000.0,10000]).T       
        self.dddq_min = -self.dddq_max

        self.data = self.model.createData()

        if q is not None:
            self.q = q
        else:
            self.q = np.zeros((self.model.nq))
        
        self.dq = np.zeros((self.model.nq))
        self.ddq = np.zeros((self.model.nq))
        self.t = np.zeros((self.model.nq))

        if is_visualisation:
            self.viz = MeshcatVisualizer(self.model, self.collision_model, self.visual_model)
            self.viz.initViewer(open=True)
            self.viz.loadViewerModel("pinocchio")

        self.is_phi_friction_torque = False # to activate friction torque, please set the gains with set_friction_gains

    # check all joint limits
    def check_joint_limits(self, q=None, dq=None, ddq=None, tau=None, dddq=None):
        if q is not None:
            q_ret = self.check_joint_position_limits(q)
        else:
            q_ret = None
        if dq is not None:
            dq_ret = self.check_joint_velocity_limits(dq)
        else:
            dq_ret = None
        if q is not None:
            ddq_ret = self.check_joint_acceleration_limits(ddq)
        else:
            ddq_ret = None
        if tau is not None:
            tau_ret = self.check_joint_effort_limits(tau)
        else:
            tau_ret = None
        if dddq is not None:
            dddq_ret = self.check_joint_jerk_limits(q)
        else:
            dddq_ret = None
        return q_ret, dq_ret, ddq_ret, tau_ret, dddq_ret
    
    def is_lim(self, x, x_max, x_min):
        '''
        Return a boolean array with value set to True for each limit reached
        '''
        return np.min(x_max - x, x - x_min) < 0

    def check_joint_position_limits(self, q:(np.array or None)=None):
        if q is None:
            return self.is_lim(self.q, self.q_max, self.q_min) 
        else:
            return self.is_lim(q, self.q_max, self.q_min)

    def check_joint_velocity_limits(self, dq:(np.array or None)=None):
        if dq is None:
            return self.is_lim(self.dq, self.dq_max, self.dq_min) 
        else:
            return self.is_lim(dq, self.dq_max, self.dq_min)

    def check_joint_acceleration_limits(self, ddq:(np.array or None)=None):
        if ddq is None:
            return self.is_lim(self.ddq, self.ddq_max, self.ddq_min) 
        else:
            return self.is_lim(ddq, self.ddq_max, self.ddq_min)

    def check_joint_jerk_limits(self, dddq):
        return self.is_lim(dddq, self.dddq_max, self.dddq_min)

    def check_joint_effort_limits(self, t):
        return self.is_lim(t, self.t_max, self.t_min)

    def apply_joint_position_limits(self, q):
        q_sat = q
        is_min = q < self.q_min
        is_max = q > self.q_max
        q_sat[is_min] = self.q_min[is_min]
        q_sat[is_max] = self.q_max[is_max]
        return q_sat

    def apply_joint_velocity_limits(self, dq):
        dq_sat = dq
        is_min = dq < self.dq_min
        is_max = dq > self.dq_max
        dq_sat[is_min] = self.dq_min[is_min]
        dq_sat[is_max] = self.dq_max[is_max]
        return dq_sat

    def apply_joint_acceleration_limits(self, ddq):
        ddq_sat = ddq
        is_min = ddq < self.ddq_min
        is_max = ddq > self.ddq_max
        ddq_sat[is_min] = self.ddq_min[is_min]
        ddq_sat[is_max] = self.ddq_max[is_max]
        return ddq_sat

    def apply_joint_jerk_limits(self, dddq):
        dddq_sat = dddq
        is_min = dddq < self.dddq_min
        is_max = dddq > self.dddq_max
        dddq_sat[is_min] = self.dddq_min[is_min]
        dddq_sat[is_max] = self.dddq_max[is_max]
        return dddq_sat

    def apply_joint_effort_limits(self, t):
        t_sat = t
        is_min = t < self.t_min
        is_max = t > self.t_max
        t_sat[is_min] = self.t_min[is_min]
        t_sat[is_max] = self.t_max[is_max]
        return t_sat

    # direct kinematics functions for 7dof
    def forward(self, q:(np.array or None)=None):
        if q is None:
            pin.framesForwardKinematics(self.model,self.data, np.array(self.q))
        else:
            pin.framesForwardKinematics(self.model,self.data, np.array(q))
        return self.data.oMf[self.tip_id]
        
    def dk_position(self, q:(np.array or None)=None):
        if q is None:
            return self.forward(self.q).translation
        else:
            return self.forward(q).translation

    def dk_orientation_matrix(self, q:(np.array or None)=None):
        if q is None:
            return self.forward(self.q).rotation
        else:
            return self.forward(q).rotation

    def jacobian(self, q:(np.array or None)=None):
        if q is None:
            pin.computeJointJacobians(self.model,self.data, np.array(self.q))
        else:
            pin.computeJointJacobians(self.model,self.data, np.array(q))
        self.Jac = pin.getFrameJacobian(self.model, self.data, self.tip_id, pin.LOCAL_WORLD_ALIGNED)
        # self.Jac = pin.getFrameJacobian(self.model, self.data, self.tip_id, pin.WORLD)
        # self.Jac = pin.getFrameJacobian(self.model, self.data, self.tip_id, pin.LOCAL)
        return np.array(self.Jac)

    def jacobian_position(self, q:(np.array or None)=None):
        if q is None:
            return self.jacobian(self.q)[:3, :] 
        else:
            return self.jacobian(q)[:3, :] 

    def jacobian_pseudo_inv(self, q:(np.array or None)=None):
        if q is None:
            self.pJac = np.linalg.pinv(self.jacobian(self.q))
        else:
            self.pJac = np.linalg.pinv(self.jacobian(q))
        return self.pJac

    def jacobian_weighted_pseudo_inv(self, q, W):
        '''
        Weighted pseudo inverse
        .. math:: J^{W+} = W^{-1} J^t (JW^{-1}J^t)^{-1}
        Args:
        - q:        joint position array
        - W:        weighting matrix (must be invertible)
        '''
        iW = np.linalg.inv(W)
        self.jacobian(q)
        self.pWJac = iW.dot(self.Jac.T).dot(np.linalg.inv( self.Jac.dot(iW).dot(self.Jac.T) ))
        return self.pWJac
    
    def jacobian_dot(self, q:(np.array or None)=None, dq:(np.array or None)=None):
        if q is None and dq is None:
            pin.computeJointJacobiansTimeVariation(self.model, self.data, self.q, self.dq)
        elif q is not None and dq is not None:
            pin.computeJointJacobiansTimeVariation(self.model, self.data, np.array(q), np.array(dq))
        elif q is None and dq is not None:
            pin.computeJointJacobiansTimeVariation(self.model, self.data, self.q, np.array(dq))
        elif q is not None and dq is None:
            pin.computeJointJacobiansTimeVariation(self.model, self.data, np.array(q), self.dq)
        self.dJac = pin.getFrameJacobianTimeVariation(self.model, self.data, self.tip_id, pin.LOCAL_WORLD_ALIGNED)
        # self.dJac = pin.getFrameJacobianTimeVariation(self.model, self.data, self.tip_id, pin.WORLD)
        # self.dJac = pin.getFrameJacobianTimeVariation(self.model, self.data, self.tip_id, pin.LOCAL)
        return self.dJac

    def gravity_torque(self, q:(np.array or None)=None):
        if q is None:
            pin.computeGeneralizedGravity(self.model,self.data, np.array(self.q))
        else:
            pin.computeGeneralizedGravity(self.model,self.data, np.array(q))
        return np.array(self.data.g)

    def mass_matrix(self, q:(np.array or None)=None):
        if q is None:
            pin.crba(self.model,self.data,np.array(self.q))
        else:
            pin.crba(self.model,self.data,np.array(q))
        return np.array(self.data.M)

    def coriolis_matrix(self, q:(np.array or None)=None, dq:(np.array or None)=None):
        if q is None and dq is None:
            pin.computeCoriolisMatrix(self.model, self.data, np.array(self.q), np.array(self.dq))
        if q is not None and dq is not None:
            pin.computeCoriolisMatrix(self.model, self.data, np.array(q), np.array(dq))
        elif q is None and dq is not None:
            pin.computeCoriolisMatrix(self.model, self.data, self.q, np.array(dq))
        elif q is not None and dq is None:
            pin.computeCoriolisMatrix(self.model, self.data, np.array(q), self.dq)
        return np.array(self.data.C)

    # fv should be identified experimentally for each robot, and each joint
    def set_viscous_friction_gain(self, fv):
        self.fv = fv

    def viscous_friction_torque(self, dq:(np.array or None)=None):
        if not hasattr(self, 'fv'):
            self.fv = np.zeros(self.model.nq)  
        if dq is None:
            return np.multiply(self.fv, self.dq)
        else:
            return np.multiply(self.fv, dq)
    
    def set_coulomb_friction_gains(self, fc, fo):
        """
        Define the gains of the Coulomb friction torques, such as:
        .. math:: \tau_{Ci}(\dot{q}_i) = f_{c,i} \sign(\dot{q}_i) + f_{o,i}
        Args:
        - fc:       Coulomb friction (np.array 1D of size equal to the nb of joints)
        - fo:       Coulomb friction offset (np.array 1D of size equal to the nb of joints)
        """
        # check inputs
        if not isinstance(fc, np.ndarray):
            fc = np.array(fc)
        if fc.ndim == 1 and fc.size == self.model.nq:
            self.fc = fc
        elif fc.ndim == 2 and (fc.shape[0] == 1 or fc.shape[1] == 1):
            self.fc = fc.squeeze()
        else:
            raise ValueError(f"fc must be a 1D array of size {self.model.nq}, according to the number of joint of the robot.")
        
        if not isinstance(fo, np.ndarray):
            fo = np.array(fo)  
        if fo.ndim == 1 and fo.size == self.model.nq:
            self.fo = fo
        elif fo.ndim == 2 and (fo.shape[0] == 1 or fo.shape[1] == 1):
            self.fo = fo.squeeze()
        else:
            raise ValueError(f"fo must be a 1D array of size {self.model.nq}, according to the number of joint of the robot.")

        self.fc = fc
        self.fo = fo

    def coulomb_friction_torque(self, dq:(np.array or None)=None):
        """
        Compute the Coulomb friction torques, such as:
        .. math:: \tau_{Ci}(\dot{q}_i) = f_{c,i} \sign(\dot{q}_i) + f_{o,i}
        Args:
        - dq:       Joint velocity (np.array)
        """
        if not hasattr(self, 'fc'):
            self.fc = np.zeros(self.model.nq)  
        if not hasattr(self, 'fo'):
            self.fo = np.zeros(self.model.nq)  
        if dq is None:
            return np.multiply(self.fv, np.sign(self.dq)) + self.fo
        else:
            return np.multiply(self.fv, np.sign(dq)) + self.fo

    def set_phi_friction_gains(self, phi1:np.ndarray[float], phi2:np.ndarray[float], phi3:np.ndarray[float], activate_friction_torque:bool=True):
        '''
        Set gains phi1, ph2, and ph3 for the computation of the torque friction (see friction_torque function).
        Args:
        - ph1:      numpy array with size according to the number of joint of the robot model
        - ph2:      numpy array with size according to the number of joint of the robot model
        - ph3:      numpy array with size according to the number of joint of the robot model
        - activate_friction_torque:     if set to True, which is the default value, the friction torque will be taken into account in the direct dynamic (boolean)
        '''
        # check inputs
        if not isinstance(phi1, np.ndarray):
            phi1 = np.array(phi1)
        if not isinstance(phi2, np.ndarray):
            phi2 = np.array(phi2)   
        if not isinstance(phi3, np.ndarray):
            phi3 = np.array(phi3)

        if phi1.ndim == 1 and phi1.size == self.model.nq:
            self.phi1 = phi1
        elif phi1.ndim == 2 and (phi1.shape[0] == 1 or phi1.shape[1] == 1):
            self.phi1 = phi1.squeeze()
        else:
            raise ValueError(f"phi1 must be a 1D array of size {self.model.nq}, according to the number of joint of the robot.")
        
        if phi2.ndim == 1 and phi2.size == self.model.nq:
            self.phi2 = phi2
        elif phi2.ndim == 2 and (phi2.shape[0] == 1 or phi2.shape[1] == 1):
            self.phi2 = phi2.squeeze()
        else:
            raise ValueError(f"phi2 must be a 1D array of size {self.model.nq}, according to the number of joint of the robot.")

        if phi3.ndim == 1 and phi3.size == self.model.nq:
            self.phi3 = phi3
        elif phi3.ndim == 2 and (phi3.shape[0] == 1 or phi3.shape[1] == 1):
            self.phi3 = phi3.squeeze()
        else:
            raise ValueError(f"phi3 must be a 1D array of size {self.model.nq}, according to the number of joint of the robot.")
        
        if activate_friction_torque:
            self.is_phi_friction_torque = True
        else:
            self.is_phi_friction_torque = False

    def friction_torque(self, dq:(np.array or None)=None):
        '''
        Friction torque as defined by Gaz et al. 2019:
        .. math:: \tau_{f,j}(\dot{q}_j)=\frac{\phi_{1,j}}{1+e^{-\phi_{2,j}(\dot{q}_j+\phi_{3,j})}} - \frac{\phi_{1,j}}{1+e^{-\phi_{2,j}\phi_{3,j}}}
        
        /!\ The set_friction_gains must be called before calling this function
        /!\ This function is not meant to be used in combination with the viscous_friction function

        (“Dynamic identification of the Franka Emika Panda robot 
        with retrieval of feasible parameters using penalty-based 
        optimization,” IEEE Robotics and Automation Letters, 2019)
        '''
        if dq is None:
            dq = self.dq

        return np.array([
            self.phi1[i]/( 1 + np.exp(-self.phi2[i]*(dqi + self.phi3[i])) ) - 
            self.phi1[i]/( 1 + np.exp(-self.phi2[i]*self.phi3[i]) ) 
            for i, dqi in enumerate(dq)])
            

    def direct_dynamic(self, q:np.array, dq:np.array, tau:np.array, f_ext:np.array) -> np.array:
        '''
        Direct dynamic model

        Arg:
        - q:        joint position array
        - dq:       joint velocity array
        - tau:      torque input array
        - f_ext:    external force array (in the global frame) f(rob->env)
        - is_friction_torque:       shall the friction_torque be used ?
        '''
        J = self.jacobian(q)
        A = self.mass_matrix(q)
        C = self.coriolis_matrix(q, dq)
        g = self.gravity_torque(q)
        tau_fv = self.viscous_friction_torque(dq)
        tau_fc = self.coulomb_friction_torque(dq)

        if self.is_phi_friction_torque:
            try:
                tau_f = self.friction_torque(dq)
            except NameError as e:
                # The error will only be printed once
                if not hasattr(self, 'phi_friction_torque_name_error_flag'):
                    print("Warning (direct_dynamic): The is_phi_friction_torque flag has been set to true, but it seems that a variable is missing, the program will continue without phi friction torques: {}".format(e))
                    self.phi_friction_torque_name_error_flag = True
                ddq = np.linalg.inv(A).dot(tau - J.T.dot(f_ext) - C.dot(dq) - g - tau_fv - tau_fc)
            except Exception as e:
                print(f"Error (direct_dynamic): {e}")
                raise e
            else:
                ddq = np.linalg.inv(A).dot(tau - J.T.dot(f_ext) - C.dot(dq) - g - tau_f)
        else:
            ddq = np.linalg.inv(A).dot(tau - J.T.dot(f_ext) - C.dot(dq) - g - tau_fv - tau_fc)

        return np.array(ddq)
    
    def direct_dynamic_aba(self, q:np.array, dq:np.array, tau:np.array, f_ext:np.array) -> np.array:
        '''
        Direct dynamic model using ABA's method (R. Featherstone 1983), with the implementation described in Analytical Derivatives
        of Rigid Body Dynamics Algorithms, by Justin Carpentier and Nicolas Mansard (http://www.roboticsproceedings.org/rss14/p38.pdf)

        Arg:
        - q:        joint position array
        - dq:       joint velocity array
        - tau:      torque input array
        - f_ext:    external force array (in the local frame) f(rob->env)
        '''
        ddq = pin.aba(model=self.model, data=self.data, q=q, v=dq, tau=tau, fext=f_ext)
        return np.array(ddq)
    
    # In progress...
    def directional_manipulability(self, q:(np.array or None)=None):
        '''
        Directional manipulability defined as a velocity transmission ratio, as defined in Virtual ergonomics for the design of collaborative robots, P. Maurice 2015 (eq 3.11)
        VTRinv = [u^t (JA^{-1}L^2A^{-1}J^t)^{-1}u]^{1/2}
        with:
            - u directional vector
            - J the jacobian matrix
            - A the inertia matrix
            - L = diag(tau_max)
        '''
        if q is None:
            J = self.jacobian(self.q)
            iM = np.linalg.inv(self.mass_matrix(self.q))
        else:
            J = self.jacobian(q)
            iM = np.linalg.inv(self.mass_matrix(q))
        tmp_man = np.linalg.inv(J.dot(iM).dot(iM).dot(J.T))
        return np.sqrt([u.T.dot(tmp_man).dot(u) for u in np.eye(6)])      

    def update_joint_data(self, q, dq:(np.ndarray or None)=None, ddq:(np.ndarray or None)=None, tau:(np.ndarray or None)=None, apply_saturation=False):
        if apply_saturation:
            self.q = self.apply_joint_position_limits(np.array(q))
            if dq is not None:
                self.dq = self.apply_joint_velocity_limits(np.array(dq))
            if ddq is not None:
                self.ddq = self.apply_joint_acceleration_limits(np.array(ddq))
            if tau is not None:
                self.t = self.apply_joint_effort_limits(np.array(tau))
        else:
            self.q = np.array(q)
            if dq is not None:
                self.dq = np.array(dq)
            if ddq is not None:
                self.ddq = np.array(ddq)
            if tau is not None:
                self.t = tau


    def update_visualisation(self, q:(np.array or None)=None):
        if q is None:
            self.viz.display(self.q)
        else:
            self.viz.display(q)

    def add_visual_object(self, obj_file_path, name:str, material=None):
        # 
        _, file_extension = os.path.splitext(obj_file_path)
        if file_extension.lower() == ".dae":
            obj = meshcat.geometry.DaeMeshGeometry.from_file(obj_file_path)
        elif file_extension.lower() == ".obj":
            obj = meshcat.geometry.ObjMeshGeometry.from_file(obj_file_path)
        elif file_extension.lower() == ".stl":
            obj = meshcat.geometry.StlMeshGeometry.from_file(obj_file_path)
        else:
            raise ImportError("Unknown mesh file format: {}.".format(file_extension))
        
        if material is None:
            material = meshcat.geometry.MeshPhongMaterial(color=0xff0000, opacity=0.4)

        self.viz.viewer[name].set_object(obj, material)