#!/usr/bin/env python3

import numpy as np
import pinocchio as pin
from typing import Tuple

class TaskController:
  # constructor - reading urdf and constructing the robots kinematic chain
  def __init__(self, ctrl_mode:str, damping_gain=None, stiffness_gain=None, nb_joint_ctrl=None):

    self.control_mode = ctrl_mode.lower() # TODO: check 

    if nb_joint_ctrl is not None:
      self.nq = nb_joint_ctrl
      self.ff_q = np.zeros(self.nq)
      self.ff_dq = np.zeros(self.nq)
      self.ff_ddq = np.zeros(self.nq)
      self.ff_tau = np.zeros(self.nq)
    else:
      if self.control_mode == 'joint' or self.control_mode == 'gravity_torque':
        print(f"Error: the number of joint controlled must be provided for the choosen control mode")
        raise ValueError

    self.ff_x = pin.SE3(np.eye(4)) # x is not provided by an 6D vector, to have a 
                          # more realiable representation of the orientation
    self.ff_dx = np.zeros(6)
    self.ff_ddx = np.zeros(6)

    if stiffness_gain is not None:
      self.setGains(damping_gain, stiffness_gain)

    # self.Kd = np.array()
    # self.Kp = np.array()

  def getTarget(self) -> Tuple[(pin.SE3 or np.ndarray), np.ndarray, np.ndarray]:
    if self.control_mode == 'endpoint':
      return self.ff_x, self.ff_dx, self.ff_ddx
    elif self.control_mode == 'joint':
      return self.ff_q, self.ff_dq, self.ff_ddq
    elif self.control_mode == 'gravity_torque':
      return self.tau_g
    else:
      raise Exception("Unknown control mode...")

  def setEndpointTarget(self, x_star:pin.SE3, dx_star:np.ndarray=None, ddx_star:np.ndarray=None):
    self.ff_x = x_star #TODO: check input

    if dx_star is None:
      self.ff_dx = np.zeros(6)
    else:
      self.ff_dx = dx_star     

    if ddx_star is None:
      self.ff_ddx = np.zeros(6)
    else:
      self.ff_ddx = ddx_star

  def setJointTarget(self, q_star, dq_star=None, ddq_star=None):
    self.ff_q = q_star
    if dq_star is None:
      self.ff_dq = np.zeros(self.nq)
    else:
      if np.array(dq_star).flatten().shape == (self.nq,):
        self.ff_dq = dq_star   
      else:
        raise Exception("MainTaskCtrl Error: joint target dimension...") # TODO..

    if ddq_star is None:
      self.ff_ddq = np.zeros(self.nq)
    else:
      if np.array(ddq_star).flatten().shape == (self.nq,):
        self.ff_ddq = ddq_star  
      else:
        raise Exception("MainTaskCtrl Error: joint velocity target dimension") # TODO..

  def setEndpointGains(self, damping_gain=None, stiffness_gain=None):
    if damping_gain is not None:
      dim = np.array(damping_gain).ndim
      if dim == 1 and np.array(damping_gain).flatten().shape == (6,):
        # gain is provided as a vector for diagonal terms
        self.Kd = np.diag(damping_gain)
      elif dim == 2 and np.array(damping_gain).shape == (6,6):
        # gain is provided as a matrix
        self.Kd = np.array(damping_gain)
      else:
        raise Exception("MainTaskCtrl Error: wrong dimension of Kd for endpoint ctrl.")

    if stiffness_gain is not None:
      dim = np.array(stiffness_gain).ndim
      if dim == 1 and np.array(stiffness_gain).flatten().shape == (6,):
        # gain is provided as a vector for diagonal terms
        self.Kp = np.diag(stiffness_gain)
      elif dim == 2 and np.array(stiffness_gain).shape == (6,6):
        # gain is provided as a matrix
        self.Kp = np.array(stiffness_gain)
      else:
        raise Exception("MainTaskCtrl Error: wrong dimension of Kp for endpoint ctrl.")

        
  def setJointGains(self, damping_gain=None, stiffness_gain=None):
    if damping_gain is not None:
      dim = np.array(damping_gain).ndim
      if dim == 1 and np.array(damping_gain).flatten().shape == (self.nq,):
        # gain is provided as a vector for diagonal terms
        self.Kd = np.diag(damping_gain)
      elif dim == 2 and np.array(damping_gain).shape == (self.nq,self.nq):
        # gain is provided as a matrix
        self.Kd = np.array(damping_gain)
      else:
        raise Exception("MainTaskCtrl Error: wrong dimension of Kd for joint ctrl.")

    if stiffness_gain is not None:
      dim = np.array(stiffness_gain).ndim
      if dim == 1 and np.array(stiffness_gain).flatten().shape == (self.nq,):
        # gain is provided as a vector for diagonal terms
        self.Kp = np.diag(stiffness_gain)
      elif dim == 2 and np.array(stiffness_gain).shape == (self.nq,self.nq):
        # gain is provided as a matrix
        self.Kp = np.array(stiffness_gain)
      else:
        raise Exception("MainTaskCtrl Error: wrong dimension of Kp for joint ctrl.")

  def setGains(self, damping_gain=None, stiffness_gain=None):
    if self.control_mode == 'endpoint':
      if damping_gain is None:
        print(f"Error: the damping gain KD must be provided for the impedance endpoint control")
        raise ValueError
      self.setEndpointGains(damping_gain, stiffness_gain)
    elif self.control_mode == 'joint':
      if damping_gain is None:
        print(f"Error: the damping gain KD must be provided for the impedance joint control")
        raise ValueError
      self.setJointGains(damping_gain, stiffness_gain)
    elif self.control_mode == 'gravity_torque':
      self.setJointGains(None, stiffness_gain)
    else:
      raise Exception("MainTaskCtrl Error: setGains, unknown control mode...")
    
  def computeEndpointImpedanceCmd(self, x:pin.SE3, dx:np.array=np.zeros(6), reference_frame:pin.ReferenceFrame=pin.LOCAL):
    xMff_x = x.actInv(self.ff_x) # 
    if reference_frame == pin.LOCAL:
      self.ddx = self.ff_ddx + self.Kd.dot(self.ff_dx - dx) + \
        self.Kp.dot( pin.log6(xMff_x).vector )  
    elif reference_frame == pin.LOCAL_WORLD_ALIGNED:
      adj = np.concatenate( (
        np.concatenate( (x.rotation, np.zeros((3,3))), axis=1 ),
        np.concatenate( (np.zeros((3,3)), x.rotation), axis=1 )
      ), axis=0 ) # action matrix when translation is null is : [R, 0; 0, R]
      self.ddx = self.ff_ddx + self.Kd.dot(self.ff_dx - dx) + \
        self.Kp.dot( adj.dot(pin.log6(xMff_x).vector) )
    elif reference_frame == pin.WORLD:
      # see https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/structpinocchio_1_1SE3Base.html#aec388721bf6853ed1fbebcf242cca30e
      adj = x.toActionMatrix() 
      self.ddx = self.ff_ddx + self.Kd.dot(self.ff_dx - dx) + \
        self.Kp.dot( adj.dot(pin.log6(xMff_x).vector) )  
    # the pose error is computed using the exponential representation of rotation
    # matrices as detailed in Modern Robotics, mechanics, planning and control
    # chapter 3.2.3.3 Matrix Logarithm of Rotations, by K.M. Lynch & F.C. Park
    return self.ddx

  def computeJointImpedanceCmd(self, q:np.ndarray, dq:np.ndarray=np.zeros(6)):
    self.ddq = self.ff_ddq + self.Kd.dot(self.ff_dq - dq) + self.Kp.dot(self.ff_q - q)
    return self.ddq
  
  # for this control mode, the target is configuration dependant and must be fed at each iteration
  def computeJointTorqueGravityCompensationCmd(self, tau_g:np.ndarray, tau:np.ndarray):
    self.tau = self.Kp.dot(tau_g - tau)
    self.tau_g = tau_g
    return self.tau