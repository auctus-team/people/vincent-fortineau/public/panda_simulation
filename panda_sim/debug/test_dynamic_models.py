# %% IMPORTS 
%matplotlib qt

import numpy as np
import math
import time

import pinocchio as pin
from matplotlib import pyplot as plt

from tqdm import tqdm # for progression bar

from panda_sim.utils.robot_solver import RobotSolver
from panda_sim.utils.task_ctrl import TaskController
from panda_sim.simulation import RK4, robot_state_ode, computeCmd
from panda_sim.sim_vs_exp import RobotState, extract_data, interpolate_data

# %% MACROS
# simulation
BASE = 'world'
TIP_LINK = 'panda_link8'
LOOP_FREQUENCY = 1e3 # Hz
# MAIN TASK GAINS
KP = np.array([1000,1000,1000,63,63,63])
KD = 2*np.sqrt(KP)
# SECONDARY TASK GAINS
KQP = np.array([100,100,100,100,100,100,100])
KQD = 2*np.sqrt(KQP)
# PHI FRICTION TORQUE COEFFICIENTS (values taken from Gaz et al (2019) supplementary data)
PHI1 = np.array([0.54615, 8722.4, 0.64068, 1.2794, 0.83904, 0.30301, 0.56489]) # N.m
PHI2 = np.array([5.1181, 9.0657, 10.136, 5.5903, 8.3469, 17.133, 10.336]) # s/rad
PHI3 = np.array([3.9533e-02, 2.5882e-02, -4.6070e-02, 3.6194e-02, 2.6226e-02, -2.1047e-02, 3.5526e-03]) # rad/s
# FV,FC&FO FRICTION TORQUE COEFFICIENTS (values taken from Gaz et al (2019) supplementary data)
FV = np.array([0.0665, 0.1987, 0.0399, 0.2257, 0.1023, -0.0132, 0.0638])
FC = np.array([0.2450, 0.1523, 0.1827, 0.3591, 0.2669, 0.1658, 0.2109])
FO = np.array([-0.1073, -0.1566, -0.0686, -0.2522, 0.0045, 0.0910, -0.0127])
#
# experimental
# should be in: panda_sim/data/ 
# FILE_NAME = "config_1/test_10N_x.csv"
FILE_NAME = "config_2/test_10N_z.csv"
ARM_ID = 'panda_1'
NB_JOINTS = 7
STEP = 0.001 # 1 ms
# time extraction
T_START = 50
T_END = 75
  
# %% FUNCTIONS
#
def read_raw_csv(file_name:str):
  file_path = "../data/" + file_name
  return np.genfromtxt(file_path, delimiter=',', names=True, deletechars='')

def plot_joint_position(exp_data:RobotState, sim_data_list:list[RobotState], labels:(str or None)=None):
  colors = plt.cm.tab10(np.linspace(0, 1, 10)) # this is not meant to be used for too many diff lines
  fig, axs = plt.subplots(2,4)
  for it in range(0,7):
    axs[math.floor(it/4),it%4].plot(exp_data.t, exp_data.q[it,:], linewidth=1.5, color=colors[0], alpha=0.8, label='Experimental')
    for i, sim_i in enumerate(sim_data_list):
      if labels is None:
        label = "Simulation {i}"
      else:
        label = labels[i]
      axs[math.floor(it/4),it%4].plot(exp_data.t, sim_i.q[it,:], linewidth=0.8, color=colors[i+1], alpha=0.8, label=label)
  
  axs[-1,-2].legend()
  axs[-1,-1].set_visible(False)
  fig.suptitle('Joint position data (m)')

#
def plot_joint_effort(exp_data:RobotState, sim_data_list:list[RobotState], labels:(str or None)=None):
  colors = plt.cm.tab10(np.linspace(0, 1, 10)) # this is not meant to be used for too many diff lines
  fig, axs = plt.subplots(2,4)
  for it in range(0,7):
    axs[math.floor(it/4),it%4].plot(exp_data.t, exp_data.tau[it,:], linewidth=1.5, color=colors[0], alpha=0.8, label='Experimental')
    for i, sim_i in enumerate(sim_data_list):
      if labels is None:
        label = "Simulation {i}"
      else:
        label = labels[i]
      axs[math.floor(it/4),it%4].plot(exp_data.t, sim_i.tau[it,:], linewidth=0.8, color=colors[i+1], alpha=0.8, label=label)
  
  axs[-1,-2].legend()
  axs[-1,-1].set_visible(False)
  fig.suptitle('Joint effort data (Nm)')

#
def plot_endpoint_position(robot:RobotSolver, exp_data:RobotState, sim_data_list:list[RobotState], title:(str or None)=None, labels:(str or None)=None):
  exp_x = np.array([pin.SE3(robot.forward(q)).translation for q in exp_data.q.T])
  sim_x = np.array([[pin.SE3(robot.forward(q)).translation for q in sim_i.q.T] for sim_i in sim_data_list])

  axis = ['X', 'Y', 'Z']
  colors = plt.cm.tab10(np.linspace(0, 1, 10)) # this is not meant to be used for too many diff lines

  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    axs[it].plot(exp_data.t, exp_x[:,it], linewidth=2, color=colors[0], alpha=0.8, label='Experimental')
    for i, _ in enumerate(sim_data_list):
      if labels is None:
        label = "Simulation {i}"
      else:
        label = labels[i]
      axs[it].plot(exp_data.t, sim_x[i,:,it], linewidth=1.5, color=colors[i+1], alpha=0.8, label=label)
    axs[it].title.set_text(axis[it] + ' axis')
    if any(exp_data.fext[it,:]):
      ax_f = axs[it].twinx()
      ax_f.plot(exp_data.t, exp_data.fext[it,:], linewidth=1.5, color='k', linestyle='dashed', alpha=0.8, label='Force Reaction')
      f_axis = axis[it]
  if f_axis == 'Z':
    axs[-2].legend()
  else:
    axs[-1].legend()
  ax_f.legend()
  if title is None:
    fig.suptitle('Endpoint position')
  else:
    fig.suptitle(title)

# %% MAIN
robot_phi = RobotSolver(BASE, TIP_LINK, is_visualisation=False)
robot_f = RobotSolver(BASE, TIP_LINK, is_visualisation=False)
robot = RobotSolver(BASE, TIP_LINK, is_visualisation=False)

robot_phi.set_phi_friction_gains(PHI1, PHI2, PHI3)
robot_f.set_coulomb_friction_gains(FC, np.zeros(robot.model.nq))
robot_f.set_viscous_friction_gain(FV)

# set up controllers gains and targets
main_task = TaskController(ctrl_mode='endpoint', damping_gain=KD, stiffness_gain=KP)
# secondary_task = TaskController(ctrl_mode='joint', nb_joint_ctrl=robot.model.nq, damping_gain=KQP, stiffness_gain=KQD)
# secondary_task = TaskController(ctrl_mode='gravity_torque', stiffness_gain=KQP, nb_joint_ctrl=robot.model.nq)

# %%
###########################
# collect experimental data
###########################
data, idxs = extract_data(read_raw_csv(FILE_NAME), ARM_ID, NB_JOINTS, set_t0=True)
data = interpolate_data(data, 0, step=STEP) # interpolation at 1ms

t_idx = 0
joint_pose_idx = list(range(1, NB_JOINTS+1))
joint_eff_idx = list(range(NB_JOINTS+1, 2*NB_JOINTS+1))
wrench_idx = list(range(2*NB_JOINTS+1, 2*NB_JOINTS+6+1)) # 6 DOF

exp_data = RobotState(NB_JOINTS, t=data[t_idx], q=data[joint_pose_idx], tau=data[joint_eff_idx], fext=data[wrench_idx])
del data
# %%
#################################
# simulate the experimental data
#################################
robot.update_joint_data(q=exp_data.q[:,0])
robot_phi.update_joint_data(q=exp_data.q[:,0])
robot_f.update_joint_data(q=exp_data.q[:,0])
sim_data = RobotState(nb_joints=7)  
sim_phi_data = RobotState(nb_joints=7)  
sim_f_data = RobotState(nb_joints=7)  

tau = exp_data.tau[:,0]
tau_phi = tau
tau_f = tau

main_task.setEndpointTarget(x_star=pin.SE3(robot.forward())) # update marker to current pose
# if secondary_task.control_mode == 'joint':
#   secondary_task.setJointTarget(q_star=robot.q) # update to current joint position

start_time = time.time()
for t in tqdm(exp_data.t, desc="Simulation  in progress..", miniters=10):
  idx = np.where(exp_data.t==t)[0].squeeze()
  #
  sim_data.add_joint_states(t, q=robot.q, dq=robot.dq, tau=tau, fext=exp_data.fext[:,idx])
  sim_phi_data.add_joint_states(t, q=robot_phi.q, dq=robot_phi.dq, tau=tau_phi, fext=exp_data.fext[:,idx])
  sim_f_data.add_joint_states(t, q=robot_f.q, dq=robot_f.dq, tau=tau_f, fext=exp_data.fext[:,idx])
  #
  tau = computeCmd(robot, main_task, is_saturation=False)
  tau_phi = computeCmd(robot_phi, main_task, is_saturation=False)
  tau_f = computeCmd(robot_f, main_task, is_saturation=False)

  dstate = RK4(robot_state_ode, np.concatenate((robot.q, robot.dq)), np.concatenate((tau, exp_data.fext[:,idx])), STEP, arg=robot)
  dstate_phi = RK4(robot_state_ode, np.concatenate((robot_phi.q, robot_phi.dq)), np.concatenate((tau_phi, exp_data.fext[:,idx])), STEP, arg=robot_phi)
  dstate_f = RK4(robot_state_ode, np.concatenate((robot_f.q, robot_f.dq)), np.concatenate((tau_f, exp_data.fext[:,idx])), STEP, arg=robot_f)
  
  robot.update_joint_data(dstate[0:robot.model.nq], dstate[robot.model.nq:], apply_saturation=False)
  robot_phi.update_joint_data(dstate_phi[0:robot.model.nq], dstate_phi[robot.model.nq:], apply_saturation=False)
  robot_f.update_joint_data(dstate_f[0:robot.model.nq], dstate_f[robot.model.nq:], apply_saturation=False)

# %%
#######################################
# compare experimental and simulated joint data
#######################################
# labels = ['sim no fric', 'sim \phi', 'sim fv+fc']
# sim_data_list = [sim_data, sim_phi_data, sim_f_data]
labels = ['sim no fric', 'sim fv+fc']
sim_data_list = [sim_data, sim_f_data]

plot_joint_position(exp_data, sim_data_list, labels=labels)
plot_joint_effort(exp_data, sim_data_list, labels=labels)

#######################################
# compute endpoint position data
#######################################
plot_endpoint_position(robot, exp_data, sim_data_list, labels=labels)

# %%
