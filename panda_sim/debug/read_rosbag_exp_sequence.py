#!/usr/bin/env python
# %% IMPORTS 
%matplotlib qt

import os
import csv
import shutil

from bagpy import bagreader
import matplotlib.pyplot as plt
from matplotlib import ticker as tck
import numpy as np

from pynocchio import RobotWrapper
from pinocchio import SE3, log6, Quaternion

# %% MACROS & GLOBALS
bag_directories = [  "/home/vincent/.ros/simulation_23_05_31/",
                     "/home/vincent/.ros/experiment0_23_05_31/",
                     "/home/vincent/.ros/experiment1_23_05_31/",
                     "/home/vincent/.ros/experiment2_23_05_31/",
                     "/home/vincent/.ros/experiment3_23_05_31/",
                     "/home/vincent/.ros/experiment4_23_05_31/",
                     "/home/vincent/.ros/experiment5_23_05_31/",
                     "/home/vincent/.ros/experiment6_23_05_31/",
                     "/home/vincent/.ros/experiment7_23_05_31/",
                     "/home/vincent/.ros/experiment8_23_05_31/",
                     "/home/vincent/.ros/experiment9_23_05_31/"]

topics = ["/panda_1/franka_state_controller/joint_states", 
          "/external_force", 
          "/panda_1/desired_cartesian_pose",
          "/panda_1/franka_state_controller/Fext"]

IS_DISPLAY = False
STEP = 1e-3 # 1 ms
KP = np.diag(500*np.ones(6))
PERT_EVAL_OFFSET = 500

# %% FUNCTIONS

def plotAVGnSTD(signals, title:str='', color='red', fig=None):
  m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
  if fig is None:
    fig, axs = plt.subplots(1,3,sharex=True)
  else:
    axs = fig.axes
  for axis in range(0,3):
    avg = np.mean(signals[:,axis,:], axis=0)
    std_dev = np.std(signals[:,axis,:], axis=0)
    # [axs[axis].plot(signal, linewidth=0.8, label='exp n{}'.format(i)) for i, signal in enumerate(signals[:,axis,:])]
    axs[axis].plot(avg, color=color, linewidth=2, label='mean')
    axs[axis].fill_between(list(range(len(avg))), avg - std_dev, avg + std_dev, color=color, alpha=0.3, label='std')
    axs[axis].yaxis.set_major_formatter(m2mm)
  axs[-1].legend()
  fig.supxlabel("Time (ms)")
  fig.supylabel("Deviation (mm)")
  fig.suptitle(title)
  plt.pause(0.05)
  return fig, axs

def computeStaticError(robot:RobotWrapper, q:np.ndarray, KP:np.ndarray, fext:np.ndarray):
  """
    .. math:: \ddot{\epsilon}^* = \ddot{x}^* - \ddot{x} = -K_D\dot{\epsilon}^* - K_P\epsilon^* + J(q) A(q)^{-1} J(q)^t f_{ext}
  Args:
    robot: robot wrapper
    q: robot joint configuration (numpy.ndarray)
    KP: proportionnal gain matrix (numpy.ndarray)
    fext: external force (only forces, not torques)
  """
  J = robot.jacobian(q)[:3,:]
  M = np.linalg.inv(KP).dot(J).dot(np.linalg.inv(robot.mass_matrix(q))).dot(J.T)
  return M.dot(fext)

# %% Data extraction

force_list_all = []
eps_list_all = []
eval_idx_all = []
sequence_name_all = []
time_list_all = []

for bag_idx, bag_directory in enumerate(bag_directories):

  if "simulation" in bag_directory:
    sim_idx = bag_idx
    static_errors_list = []

  force_list = []
  eps_list = []
  eval_idx = []
  sequence_name = []
  time_list = []

  bag_files = np.array([file for file in os.listdir(bag_directory) if file.startswith("config_n")])

  for file_nb, bag_file_name in enumerate(bag_files):
    bag_file = bagreader(os.path.join(bag_directory, bag_file_name), verbose=False)
    # create csv from rosbag
    joint_state_msg = bag_file.message_by_topic(topics[0])
    external_force_msg = bag_file.message_by_topic(topics[1])
    desired_pose_msg = bag_file.message_by_topic(topics[2])  
    # read csv
    raw_data_force = np.genfromtxt(external_force_msg, delimiter=',', names=True, deletechars='')
    raw_data_joint = np.genfromtxt( ("\t".join(i) for i in csv.reader(open(joint_state_msg))), delimiter="\t", names=True )
    raw_data_des_pose = np.genfromtxt(desired_pose_msg, delimiter=',', names=True, deletechars='')

    shutil.rmtree(os.path.dirname(joint_state_msg)) # delete the folder created with the csv
    # dir_name = os.path.basename(os.path.normpath(os.path.dirname(joint_state_msg)))

    # get force torque data
    ft_time = np.array([data[0] for data in raw_data_force])
    force = np.array([np.array(data.tolist())[5:8] for data in raw_data_force]).T
    torque = np.array([np.array(data.tolist())[8:11] for data in raw_data_force]).T
    # get joint data
    q_time = np.array([np.array(data.tolist())[0] for data in raw_data_joint])
    q = np.array([np.array(data.tolist())[6:13] for data in raw_data_joint]).T
    # get target pose data
    x_d_time = np.array([np.array(data.tolist())[0] for data in raw_data_des_pose])
    position_des = np.array([np.array(data.tolist())[5:8] for data in raw_data_des_pose]).T
    orientation_des = np.array([np.array(data.tolist())[8:13] for data in raw_data_des_pose]).T

    # data interpolation
    time = np.arange(min(np.concatenate((ft_time, q_time))), max(np.concatenate((ft_time, q_time))), STEP)
    force = np.array([np.interp(time, ft_time, data) for data in force])
    torque = np.array([np.interp(time, ft_time, data) for data in torque])
    q = np.array([np.interp(time, q_time, data) for data in q])
    position_des = np.array([np.interp(time, x_d_time, data) for data in position_des])
    orientation_des = np.array([np.interp(time, x_d_time, data) for data in orientation_des])

    # load robot model
    ws_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
    panda = RobotWrapper("panda_link8", os.path.join(ws_path, "src/franka_ros/franka_description/panda.urdf"))

    # compute poses
    pose = [SE3(panda.forward(data)) for data in q.T]
    target_pose = [SE3(Quaternion(np.array(wi)).toRotationMatrix(), np.array(ti)) for ti, wi in zip(position_des.T, orientation_des.T)]

    # compute servoing errors
    eps = []
    for pose_i, target_pose_i in zip(pose,target_pose):
      adj = np.concatenate( (np.concatenate( (pose_i.rotation, np.zeros((3,3))), axis=1 ),np.concatenate( (np.zeros((3,3)), pose_i.rotation), axis=1 )), axis=0 )
      eps.append(adj@log6(pose_i.actInv(target_pose_i)).vector)
    eps = np.array(eps).T
    # estimate servoing errors
    if sim_idx == bag_idx:
      static_errors = np.array([computeStaticError(panda, qi, KP[:3,:3], fi) for qi, fi in zip(q.T, force.T)]).T
      static_errors_list.append(static_errors)

    # Compute errors between measure and estimation
    # Getting first edge index of the force perturbation
    pert_idx = np.where(np.abs(np.diff(force))>0)[1][0]
    # err convergence
    # final_idx = np.nan
    # err_i_old = np.zeros(3)
    # for idx, err_i in enumerate(err.T):
    #   conv_crit = np.array(abs((err_i - err_i_old)/err_i))
    #   err_i_old = err_i
    #   if all(conv_crit <= 1e-3):
    #     final_error = err_i
    #     final_idx = idx
    #     break

    # err = eps[0:3,pert_idx-1:] - static_errors[:,pert_idx-1:]
    # final_error = err[:,PERT_EVAL_OFFSET]
    # 
    # err_list.append(final_error)
    force_list.append(force)
    eps_list.append(eps)
    eval_idx.append(pert_idx+PERT_EVAL_OFFSET)
    sequence_name.append(bag_file_name[:-24])
    time_list.append(time)

    print("File n°{}/{}".format(file_nb+1, bag_files.shape[0]))
  print("### Experiment n°{}/{}".format(bag_idx+1, len(bag_directories)))
  # err_list_all.append(err_list)
  force_list_all.append(force_list)
  eps_list_all.append(eps_list)
  eval_idx_all.append(eval_idx)
  sequence_name_all.append(sequence_name)
  time_list_all.append(time_list)

# %% computing the estimation err

final_err_list_all = []
for ses in range(len(eps_list_all)):
  final_err_list = []
  for exp in range(len(eps_list_all[0])):
    
    # pert_idx = eval_idx_all[ses][exp] - PERT_EVAL_OFFSET
    # pert_idx_sim = eval_idx_all[sim_idx][exp] - PERT_EVAL_OFFSET
    # err = eps_list_all[ses][exp][0:3,pert_idx-1:] - static_errors[:,pert_idx_sim-1:]

    final_err = eps_list_all[ses][exp][0:3,eval_idx_all[ses][exp]] - static_errors[:,eval_idx_all[sim_idx][exp]]
    final_err_list.append(final_err)

  final_err_list_all.append(final_err_list)

final_err_list_all = np.array(final_err_list_all)

# %% converting to np array

# err_list_all = np.array(err_list_all)
eval_idx_all = np.array(eval_idx_all)
sequence_name_all = np.array(sequence_name_all)

# mapping between the experimental and simulated data
# mapping_idx = np.array([(i, j) for i, x in enumerate(sequence_name_all[0]) for j, y in enumerate(sequence_name_all[1]) if x == y])
mapping_idx = np.zeros((len(sequence_name_all), len(sequence_name_all[0])), int)
for i in range(len(sequence_name_all)):
  for j in range(len(sequence_name_all[0])):
    if i != sim_idx:
      mapping_idx[i][j] = np.where(sequence_name_all[i] == sequence_name_all[sim_idx][j])[0][0]

mapping_idx[sim_idx] = np.array(range(0,len(sequence_name_all[sim_idx])))

# %% making homogenous data

dim_data = np.array([[len(data[0]) for data in session] for session in force_list_all])
force_list_hom_all = []
eps_list_hom_all = []
for ses, force_sess in enumerate(force_list_all):
  force_list_hom = []
  eps_list_hom = []
  for exp, force in enumerate(force_sess):
    idx_pert = eval_idx_all[ses][exp]
    force_list_hom.append(np.array(force)[:,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+2500])
    eps_list_hom.append(eps_list_all[ses][exp][:3,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+2500])
  force_list_hom_all.append(force_list_hom)
  eps_list_hom_all.append(eps_list_hom)

force_list_hom_all = np.array(force_list_hom_all)
eps_list_hom_all = np.array(eps_list_hom_all)

static_errors_list_hom = []
for exp, static_err in enumerate(static_errors_list):
  idx_pert = eval_idx_all[sim_idx][exp]
  static_errors_list_hom.append(np.array(static_err)[:,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+2500])

static_errors_list_hom = np.array(static_errors_list_hom)

# %% plot std and mean of a repeated perturbation test

config_names = ['config_n1_40N_z', 'config_n1_25N_z', 'config_n1_10N_z']
title = 'config 1 (fz)'
ampl_list = ['40N', '25N', '10N']
color_list = ['red', 'blue', 'green']

fig = None
for i, config_name in enumerate(config_names):
  # indices = [i for i, string in enumerate(sequence_name_all[sim_idx]) if '40N_z' in string]
  indice = [i for i, string in enumerate(sequence_name_all[sim_idx]) if config_name in string][0]

  selected_eps = np.array([eps_list[mapping_idx[idx,indice]] for idx, eps_list in enumerate(eps_list_hom_all)])
  selected_f = np.array([eps_list[mapping_idx[idx,indice]] for idx, eps_list in enumerate(eps_list_hom_all)])

  non_sim_idx = [i for i in range(selected_eps.shape[0]) if i != sim_idx]

  plt.rcParams.update({'font.size': 16})
  fig, axs = plotAVGnSTD(selected_eps[non_sim_idx,:,:], title=title, color=color_list[i], fig=fig)

  # Add the gazebo simulation result, and the prediction
  for axis in range(0,3):
    axs[axis].plot(selected_eps[sim_idx,axis], label="gazebo " + ampl_list[i], color=color_list[i], linestyle='dashed')
    axs[axis].plot(static_errors_list_hom[indice,axis], label="pred " + ampl_list[i], color=color_list[i], linestyle='dotted')
    axs[axis].plot(PERT_EVAL_OFFSET+250, static_errors_list_hom[indice, axis, PERT_EVAL_OFFSET+250], "*", color=color_list[i], markersize=10)
  axs[-1].legend()

fig.tight_layout()

# %%

for i, exp_name in enumerate(sequence_name_all[0]):
  if '40N_z' in exp_name:
    m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
    sim_idx = mapping_idx[i][1]
    fig, axs = plt.subplots(2,3,sharex=True)
    for axis in range(0,3):
      axs[0, axis].plot(force_list_all[0][i][axis], label='panda')
      axs[0, axis].plot(force_list_all[1][sim_idx][axis], label='gazebo')
      axs[1, axis].plot(static_errors_list_all[0][i][axis], label='estimation')
      axs[1, axis].plot(eps_list_all[0][i][axis], linestyle='dotted', label='panda')
      axs[1, axis].plot(eps_list_all[1][sim_idx][axis], linestyle='dashed', label='gazebo')
      axs[1, axis].plot(eval_idx_all[0][i], eps_list_all[0][i][axis, eval_idx_all[0][i]], "*")
      axs[1, axis].plot(eval_idx_all[1][sim_idx], eps_list_all[1][sim_idx][axis, eval_idx_all[1][sim_idx]], "o")
      axs[1, axis].yaxis.set_major_formatter(m2mm)
    axs[-1,-1].legend()
    fig.supxlabel("Time (milliseconds)")
    fig.suptitle(bag_file_name[:-24])
    plt.pause(0.05)

# %%
axes = ["X", "Y", "Z"]
fig, axs = plt.subplots(1,3,sharex=True)
for axis in range(0,3):
  axs[axis].plot(np.array(err_list)[:,axis], label='err')
  # axs[axis].plot(np.array(err_list_2)[:,axis], label='err - mean')
  axs[axis].title.set_text(axes[axis])
axs[-1].legend()
fig.supxlabel("Exp nb")
fig.suptitle("Error statistics")
# %%

print("Mean absolute errors: {:.3f}mm".format(np.mean(np.abs(err_list))*1000))
print("Mean errors: {:.3f}mm".format(np.mean(err_list)*1000))
print("Standard deviation errors: {:.3f}mm".format(np.std(err_list)*1000))

# %%
