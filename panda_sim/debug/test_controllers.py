# %% IMPORTS 
%matplotlib qt

import numpy as np
import copy

import pinocchio as pin
from matplotlib import pyplot as plt

from tqdm import tqdm # for progression bar

from panda_sim.sim_vs_exp import RobotState
from panda_sim.simulation import RK4, robot_state_ode, computeCmd, computeTorqueQPCmd, force_pert_seq
from panda_sim.utils.robot_solver import RobotSolver
from panda_sim.utils.task_ctrl import TaskController
from panda_sim.utils.torque_qp import TorqueQP

# %% MACROS
# simulation
BASE = 'world'
TIP_LINK = 'panda_link8'
LOOP_FREQUENCY = 1e3 # Hz
# MAIN TASK GAINS
KP = np.array([1000,1000,1000,63,63,63])
KD = 2*np.sqrt(KP)
# SECONDARY TASK GAINS
KQP = np.array([100,100,100,100,100,100,100])
KQD = 2*np.sqrt(KQP)
# REGULARISATION TASK GAINS
KP_qp = 10*np.ones(7) # for velocity proportionnal gain
RW = 1e-5 # regularisation weight
#
INITIAL_CONFIG = np.array([np.pi/2,-0.1871,0,-2.0942,0,1.90707,0])
NB_JOINTS = 7
STEP = 0.001 # 1 ms
T_MAX = 8

# %% FUNCTIONS

def plot_endpoint_position(robot:RobotSolver, sim_data_list:list[RobotState], title:(str or None)=None, labels:(str or None)=None):
  sim_x = np.array([[pin.SE3(robot.forward(q)).translation for q in sim_i.q.T] for sim_i in sim_data_list])

  axis = ['X', 'Y', 'Z']
  colors = plt.cm.tab10(np.linspace(0, 1, 10)) # this is not meant to be used for too many diff lines

  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    for i, sim_i in enumerate(sim_data_list):
      if labels is None:
        label = "Simulation {i}"
      else:
        label = labels[i]
      axs[it].plot(sim_i.t, sim_x[i,:,it], linewidth=1.5, color=colors[i+1], alpha=0.8, label=label)
    axs[it].title.set_text(axis[it] + ' axis')
  axs[-1].legend()
  if title is None:
    fig.suptitle('Endpoint position')
  else:
    fig.suptitle(title)

# %% MAIN
######################
robot_ctrl1 = RobotSolver(BASE, TIP_LINK, q=INITIAL_CONFIG, is_visualisation=False)
robot_ctrl2 = RobotSolver(BASE, TIP_LINK, q=INITIAL_CONFIG, is_visualisation=False)

f_ext = np.zeros(6)
f_mag = 10 # force cube side (N), for polytopic representation

tau_ctrl1 = robot_ctrl1.gravity_torque()
tau_ctrl2 = robot_ctrl2.gravity_torque()

robot_ctrl1.update_joint_data(q=INITIAL_CONFIG, tau=tau_ctrl1)
robot_ctrl2.update_joint_data(q=INITIAL_CONFIG, tau=tau_ctrl2)

marker_pose = copy.copy(pin.SE3(robot_ctrl1.forward()))

# set up controllers gains and targets
main_task = TaskController(ctrl_mode='endpoint', damping_gain=KD, stiffness_gain=KP)
main_task.setEndpointTarget(x_star=marker_pose, dx_star=np.zeros(6), ddx_star=np.zeros(6))
main_task_qp = copy.deepcopy(main_task)
# for the pure model compensation controller
secondary_task = TaskController(ctrl_mode='joint',nb_joint_ctrl=robot_ctrl1.model.nq)
secondary_task.setJointTarget(q_star=INITIAL_CONFIG, dq_star=np.zeros(robot_ctrl1.model.nq), ddq_star=np.zeros(robot_ctrl1.model.nq))
secondary_task.setGains(damping_gain=KQP, stiffness_gain=KQD)
# for the qp controller
torque_qp = TorqueQP(robot_ctrl2, dt=1./LOOP_FREQUENCY, k_reg=KP_qp, rw=RW, solver='qpoases')

time = np.arange(0,T_MAX,STEP)
ctrl1_data = RobotState(nb_joints=7)  
ctrl2_data = RobotState(nb_joints=7)  

# %% MAIN LOOP

for t in tqdm(time, desc="Simulation  in progress..", miniters=10):
  # external force
  f_ext = force_pert_seq(t)

  ctrl1_data.add_joint_states(t, q=robot_ctrl1.q, dq=robot_ctrl1.dq, tau=tau_ctrl1, fext=f_ext)
  ctrl2_data.add_joint_states(t, q=robot_ctrl2.q, dq=robot_ctrl2.dq, tau=tau_ctrl2, fext=f_ext)

  tau_ctrl1 = computeCmd(robot_ctrl1, main_task, secondary_task, is_saturation=False)
  tau_ctrl2 = computeTorqueQPCmd(robot_ctrl2, main_task_qp, torque_qp, is_saturation=False)

  dstate_ctrl1 = RK4(robot_state_ode, np.concatenate((robot_ctrl1.q, robot_ctrl1.dq)), np.concatenate((tau_ctrl1, f_ext)), STEP, arg=robot_ctrl1)
  dstate_ctrl2 = RK4(robot_state_ode, np.concatenate((robot_ctrl2.q, robot_ctrl2.dq)), np.concatenate((tau_ctrl2, f_ext)), STEP, arg=robot_ctrl2)

  robot_ctrl1.update_joint_data(dstate_ctrl1[0:robot_ctrl1.model.nq], dstate_ctrl1[robot_ctrl1.model.nq:], apply_saturation=False)
  robot_ctrl2.update_joint_data(dstate_ctrl2[0:robot_ctrl2.model.nq], dstate_ctrl2[robot_ctrl2.model.nq:], apply_saturation=False)

  # %% PLOT

labels = ['sim', 'sim qp']
sim_data_list = [ctrl1_data, ctrl2_data]

#######################################
# compute endpoint position data
#######################################
plot_endpoint_position(robot_ctrl1, sim_data_list, labels=labels)
# %%
