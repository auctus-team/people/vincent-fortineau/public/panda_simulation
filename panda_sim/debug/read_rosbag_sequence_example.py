#!/usr/bin/env python
# %% IMPORTS 
%matplotlib qt

import os
import csv
import math
from scipy.signal import sosfiltfilt, butter
from scipy.spatial.transform import Rotation as Rot

from bagpy import bagreader
import matplotlib.pyplot as plt
import numpy as np
import copy as cp

from pynocchio import RobotWrapper
from pinocchio import SE3, log6, Quaternion

# %% MACROS & GLOBALS
bag_filename = '/home/vincent/.ros/experiment_23_05_26/config_n1_40N_z_2023-05-26-12-21-28.bag'
# bag_filename = '/home/vincent/.ros/config_n0_10N_x_2023-05-22-14-53-35.bag'
# bag_filename = '/home/vincent/.ros/config_n1_40N_z_2023-05-22-12-24-03.bag' 
# bag_filename = '/home/vincent/.ros/config_n0_10N_x_2023-05-16-16-27-07.bag'
topics = ["/panda_1/franka_state_controller/joint_states", 
          "/external_force", 
          "/panda_1/desired_cartesian_pose"]
STEP = 1e-3 # 1 ms
KP = np.diag(1000*np.ones(6))

# %% FUNCTIONS

def computeStaticError(robot:RobotWrapper, q:np.ndarray, KP:np.ndarray, fext:np.ndarray):
  """
    .. math:: \ddot{\epsilon}^* = \ddot{x}^* - \ddot{x} = -K_D\dot{\epsilon}^* - K_P\epsilon^* + J(q) A(q)^{-1} J(q)^t f_{ext}
  Args:
    robot: robot wrapper
    q: robot joint configuration (numpy.ndarray)
    KP: proportionnal gain matrix (numpy.ndarray)
    fext: external force (only forces, not torques)
  """
  J = robot.jacobian(q)[:3,:]
  M = np.linalg.inv(KP).dot(J).dot(np.linalg.inv(robot.mass_matrix(q))).dot(J.T)
  return M.dot(fext)

# %% Data extraction

path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)

bag = bagreader(bag_filename)

joint_state_msg = bag.message_by_topic(topics[0])
external_force_msg = bag.message_by_topic(topics[1])
desired_pose_msg = bag.message_by_topic(topics[2])
# joint_desired_state_msg = bag.message_by_topic(topics[2])
# try:
#   pose_msg = bag.message_by_topic(topics[2])
# except Exception:
#   print("No cartesian pose in this rosbag")

raw_data_force = np.genfromtxt(external_force_msg, delimiter=',', names=True, deletechars='')
raw_data_joint = np.genfromtxt( ("\t".join(i) for i in csv.reader(open(joint_state_msg))), delimiter="\t", names=True )
raw_data_des_pose = np.genfromtxt(desired_pose_msg, delimiter=',', names=True, deletechars='')
# raw_data_desired_joint = np.genfromtxt( ("\t".join(i) for i in csv.reader(open(joint_desired_state_msg))), delimiter="\t", names=True )
# raw_data_joint = np.genfromtxt(joint_state_msg, delimiter=',', names=True, deletechars='')
# try:
#   raw_data_pose = np.genfromtxt(pose_msg, delimiter=',', names=True, deletechars='')
# except Exception: 
#   pass

f_time = np.array([data[0] for data in raw_data_force])
fx = np.array([data[5] for data in raw_data_force])
fy = np.array([data[6] for data in raw_data_force])
fz = np.array([data[7] for data in raw_data_force])
force = np.array([fx, fy, fz])
tx = np.array([data[8] for data in raw_data_force])
ty = np.array([data[9] for data in raw_data_force])
tz = np.array([data[10] for data in raw_data_force])
torque = np.array([tx, ty, tz])

panda = RobotWrapper("panda_link8", os.path.join(path, "src/franka_ros/franka_description/panda.urdf"))

p_time = np.array([np.array(data.tolist())[0] for data in raw_data_joint])
joint_position = np.array([np.array(data.tolist())[6:13] for data in raw_data_joint]).T
# p_des_time = np.array([np.array(data.tolist())[0] for data in raw_data_desired_joint])
# joint_desired_position = np.array([np.array(data.tolist())[6:13] for data in raw_data_desired_joint]).T
position = np.array([cp.deepcopy(panda.dk_position(data)) for data in joint_position.T]).T

p_des_time = np.array([np.array(data.tolist())[0] for data in raw_data_des_pose])
position_des = np.array([np.array(data.tolist())[5:8] for data in raw_data_des_pose]).T
orientation_des = np.array([np.array(data.tolist())[8:13] for data in raw_data_des_pose]).T

# try:
#   ee_p_time = np.array([data[0] for data in raw_data_pose])
#   px = np.array([data[5] for data in raw_data_pose])
#   py = np.array([data[6] for data in raw_data_pose])
#   pz = np.array([data[7] for data in raw_data_pose])
#   endpoint_pose = np.array([px, py, pz])
# except Exception:
#   pass

# %% Data interpolation 

t_min = min(np.concatenate((f_time, p_time)))
t_max = max(np.concatenate((f_time, p_time)))
time = np.arange(t_min, t_max, STEP)

force = np.array([np.interp(time, f_time, data) for data in force])
torque = np.array([np.interp(time, f_time, data) for data in torque])
position = np.array([np.interp(time, p_time, data) for data in position]) 
joint_position = np.array([np.interp(time, p_time, data) for data in joint_position]) 
position_des = np.array([np.interp(time, p_des_time, data) for data in position_des])
orientation_des = np.array([np.interp(time, p_des_time, data) for data in orientation_des])

# joint_desired_position = np.array([np.interp(time, p_des_time, data) for data in joint_desired_position]) 

# endpoint_pose = np.array([np.interp(time, ee_p_time, data) for data in endpoint_pose])

# %% Compute servoing error
pose = [SE3(cp.deepcopy(panda.forward(data))) for data in joint_position.T]
# pose_desired = [SE3(cp.deepcopy(panda.forward(data))) for data in joint_desired_position.T]
pose_desired = [SE3(Quaternion(np.array(wi)).toRotationMatrix(), np.array(ti)) for ti, wi in zip(position_des.T, orientation_des.T)]

eps = []
for pose_i, pose_desired_i in zip(pose,pose_desired):
  adj = np.concatenate( (np.concatenate( (pose_i.rotation, np.zeros((3,3))), axis=1 ),np.concatenate( (np.zeros((3,3)), pose_i.rotation), axis=1 )), axis=0 )
  eps.append(adj@log6(pose_i.actInv(pose_desired_i)).vector)
eps = np.array(eps).T

# %% Data filtering

sos = butter(2, 15, output='sos', btype='lowpass', fs=1/STEP)
position_filt = sosfiltfilt(sos, position)

# %% Compute estimated errors
static_errors = np.array([computeStaticError(panda, qi, KP[:3,:3], fi) for qi, fi in zip(joint_position.T, force.T)]).T

# %% Getting first edge index
pert_idx = np.where(np.abs(np.diff(force))>0)[1][0]

# %% Compute errors between measure and estimation
conv_crit = 1000
final_idx = np.nan
err = eps[0:3,pert_idx-1:] - static_errors[:,pert_idx-1:]
err_i_old = np.zeros(3)
for idx, err_i in enumerate(err.T):
  conv_crit = np.array(abs((err_i - err_i_old)/err_i))
  err_i_old = err_i
  if all(conv_crit <= 1e-3):
    final_error = err_i
    final_idx = idx
    break

# %% Display

fig, axs = plt.subplots(2,4)
for it in range(0,7):
  axs[math.floor(it/4),it%4].plot(time, joint_position[it], linewidth=1.5, color='r', alpha=0.8)
  # axs[math.floor(it/4),it%4].plot(time, joint_data[it], linewidth=1.5, color='b', linestyle='dashed', alpha=0.8)
axs[-1,-1].set_visible(False)
fig.suptitle('Joint position data (m)')
fig.supxlabel("Time (seconds)")

fig, axs = plt.subplots(2,3,sharex=True)
for axis in range(0,3):
  axs[0, axis].plot(time, force[axis])
  # axs[1, axis].plot(time, np.mean(position[axis,:100]) - position[axis], label='\epsilon')
  # axs[1, axis].plot(time, np.mean(position_filt[axis,:100]) - position_filt[axis], label='\epsilon filt')
  axs[1, axis].plot(time, static_errors[axis], label='\hat{\epsilon}')
  axs[1, axis].plot(time, eps[axis], linestyle='dotted', label='eps')
  axs[1, axis].plot(time[pert_idx+final_idx], eps[axis, pert_idx+final_idx], "*")
  # axs[1, axis].plot(time, endpoint_pose[axis], label='from topic', alpha=0.4)
axs[-1,-1].legend()
fig.supxlabel("Time (seconds)")

# %%
