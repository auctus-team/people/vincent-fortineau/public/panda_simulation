# %% IMPORTS 
%matplotlib qt

import numpy as np
import scipy.signal as sig

import pinocchio as pin
from matplotlib import pyplot as plt
from matplotlib import ticker as tck

from panda_sim.utils.robot_solver import RobotSolver
from panda_sim.utils.task_ctrl import TaskController
from panda_sim.utils.static_error_solver import RobotStaticErrorSolver
from panda_sim.simulation import RK4, robot_state_ode, computeCmd

# %% MACROS
BASE = 'world'
TIP_LINK = 'panda_link8'
LOOP_FREQUENCY = 1e3 # Hz
T_MAX = 5
NB_CONFIG = 100 # number of random config to be tested
TOLERANCE = 1e-3 # for the convergence criteria
# MAIN TASK GAINS
KP = np.array([1000,1000,1000,63,63,63])
KD = 2*np.sqrt(KP)
# SECONDARY TASK GAINS
KQP = np.array([100,100,100,100,100,100,100])
KQD = 2*np.sqrt(KQP)

# %% FUNCTIONS

def plot(est_err_vec, stat_err_vec, manipul_vec):
  axes_name = ['x', 'y', 'z']
  m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    axs[it].plot(np.abs(est_err_vec[:,it]), linewidth=0.8, color='r', alpha=0.8)
    axs[it].set_title(axes_name[it] + ' error (mm)')
    axs[it].yaxis.set_major_formatter(m2mm)
  fig.suptitle('Estimation errors: $|\hat{\epsilon}_s - \lim_{t->\infty}\epsilon(t)|$, against random configurations')

  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    axs[it].plot(manipul_vec[:,it], stat_err_vec[:,it], marker=".", linestyle = 'None', color='r', alpha=0.8)
    axs[it].set_title(axes_name[it] + ' static error (mm)')
    axs[it].yaxis.set_major_formatter(m2mm)
  fig.suptitle('Static error $\epsilon_s$ against manipulability')


def debug_main_config():
  robot = RobotSolver(BASE, TIP_LINK, is_visualisation=False)

  # set up controllers gains and targets
  main_task = TaskController(ctrl_mode='endpoint')
  main_task.setGains(damping_gain=KD, stiffness_gain=KP)
  secondary_task = TaskController(ctrl_mode='joint',nb_joint_ctrl=robot.model.nq)
  secondary_task.setGains(damping_gain=KQP, stiffness_gain=KQD)

  dt = 1./LOOP_FREQUENCY
  time = np.arange(0, T_MAX, dt)
  random_configs = np.array([np.random.uniform(robot.q_min[idx], robot.q_max[idx], size=NB_CONFIG) for idx in range(robot.model.nq)]).T

  endpnt_err = RobotStaticErrorSolver(robot, 'endpoint_impedance')
  old_estimation_error = np.inf # for convergence criteria
  # vector
  final_error = []
  static_error = []
  manipulability = []

  # MAIN LOOP
  for config in random_configs:
    robot.update_joint_data(config, np.zeros((robot.model.nq)))
    f_ext = np.zeros((6))
    manipulability.append(robot.directional_manipulability())
    # update controllers according to new configuration
    main_task.setEndpointTarget(x_star=pin.SE3(robot.forward()), dx_star=np.zeros(6), ddx_star=np.zeros(6))
    secondary_task.setJointTarget(q_star=robot.q, dq_star=np.zeros(robot.model.nq), ddq_star=np.zeros(robot.model.nq))
    # eps_vec = []
    # static_eps_vec = []
    # conv_crit_vec = []
    # conv_reached = False
    # idx_conv = int(time[-1]*LOOP_FREQUENCY)
    #
    for t in time:
      tau = computeCmd(robot, main_task, secondary_task, is_saturation=False)
      dstate = RK4(robot_state_ode, np.concatenate((robot.q, robot.dq)), np.concatenate((tau, f_ext)), dt, arg=robot) 
      robot.update_joint_data(dstate[0:robot.model.nq], dstate[robot.model.nq:], apply_saturation=False)
      
      x = pin.SE3(robot.forward())
      # movement error induced by the external force
      # eps = main_task.ff_x.translation - x.translation
      # adj for local world aligned reference frame (adj = [R, 0; 0, R])
      adj = np.concatenate( (np.concatenate( (x.rotation, np.zeros((3,3))), axis=1 ),np.concatenate( (np.zeros((3,3)), x.rotation), axis=1 )), axis=0 )
      eps = adj.dot(pin.log6(x.actInv(main_task.ff_x)).vector)
      static_eps = endpnt_err.computeSE(robot.q, np.diag(KP), f_ext) # estimated static error
      estimation_error = static_eps - eps
      conv_crit = abs((estimation_error - old_estimation_error)/estimation_error)
      #
      # eps_vec.append(eps)
      # static_eps_vec.append(static_eps)
      # conv_crit_vec.append(conv_crit)
      if all(i <= TOLERANCE for i in conv_crit): # and not conv_reached:
        final_error.append(estimation_error)
        static_error.append(static_eps)
        print('Convergence reached for configuration: [{}] at t={}s'.format(config, t))
        break
        # conv_reached = True
        # idx_conv = int(t*LOOP_FREQUENCY)

      else:
        old_estimation_error = estimation_error
      if t == 0:
        f_ext = np.array([0.,0.,10,0.,0.,0.])
      elif t == time[-1]:
        final_error.append(estimation_error)
        static_error.append(static_eps)
        print('Convergence not reached for configuration: [{}]'.format(config))
    # plot error displacements induced by external forces
    # eps_vec = np.array(eps_vec)
    # static_eps_vec = np.array(static_eps_vec)
    # conv_crit_vec = np.array(conv_crit_vec)
    # axes_name = ['x', 'y', 'z']
    # fig, axs = plt.subplots(2,3)
    # for it in range(0,3):
    #   axs[0,it].plot(time, eps_vec[:,it], linewidth=0.8, color='r', alpha=0.8, label="$\epsilon$")
    #   axs[0,it].plot(time, static_eps_vec[:,it], linewidth=0.8, color='b', alpha=0.8, label="$\epsilon_s$")
    #   axs[0,it].set_title(axes_name[it] + ' error (m)')
    #   axs[1,it].plot(time, conv_crit_vec[:,it], linewidth=0.8, color='r', alpha=0.8)
    #   axs[1,it].plot(time[idx_conv], conv_crit_vec[idx_conv,it], linewidth=None, color='k', alpha=0.8, marker='*')
    #   axs[1,it].set_title(axes_name[it] + ' convergence criteria')
    # axs[0,2].legend()
    # fig.suptitle('Force-displacement')

  plot(np.array(final_error), np.array(static_error), np.array(manipulability))

# %% MAIN
if __name__ == "__main__":
  debug_main_config()

# %%
