#!/usr/bin/env python
# %% IMPORTS
from pinocchio import SE3, log6
import pinocchio as pin
from pynocchio import RobotWrapper
# from pykin.robots.single_arm import SingleArm
import meshcat
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import os
%matplotlib qt


# %% MACROS

REAL_LENGTH = 0.390
MARGINS = 0.02
LENGTH = REAL_LENGTH - MARGINS

# ISOCUBE_VERTICES = [[-LENGTH/2, 0, 0], [-LENGTH/2, LENGTH, 0], [LENGTH/2, LENGTH, 0], [LENGTH/2, 0, 0],
#                     [-LENGTH/2, 0, LENGTH], [-LENGTH/2, LENGTH, LENGTH], [LENGTH/2, LENGTH, LENGTH], [LENGTH/2, 0, LENGTH]]
ISOCUBE_VERTICES = [[0, -LENGTH/2, 0], [LENGTH, -LENGTH/2, 0], [LENGTH, LENGTH/2, 0], [0, LENGTH/2, 0],
                    [0, -LENGTH/2, LENGTH], [LENGTH, -LENGTH/2, LENGTH], [LENGTH, LENGTH/2, LENGTH], [0, LENGTH/2, LENGTH]]

OFFSETS = [0.20, 0, 0.10] # to avoid collision from the table and the base
ISOCUBE_VERTICES = np.array(ISOCUBE_VERTICES) + np.array(OFFSETS)

# %% FUNCTIONS

def ik_with_margin(robot:RobotWrapper, oMdes:pin.SE3, q:(np.ndarray or None)=None, qlim:bool=False, verbose:bool=True, iterations:int=1000, precision:float=1e-4, tries:int = 2, margin:float=0) -> np.ndarray:
  """
  Iterative inverse kinematics based on the example code from
  https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html

  Args:
      oMdes: SE3 matrix expressed in the world frame of the robot's endefector desired pose    
      q: currrent robot configuration (default robot's neutral position)
      verbose: bool variable enabling verbose ouptut (default True)

  Returns
  --------
      q_des: robot configuration corresponding to the desired pose 
  """
  data_ik  = robot.model.createData()

  if q is None:
    q = (robot.q_min+robot.q_max)/2

  # ik parameters
  eps    = precision
  IT_MAX = iterations
  DT     = 1e-1
  damp   = 1e-12
  margin = abs(margin)

  for t in range(tries):

      if verbose:
          print("Try: %d" % t)

      if t> 0:
          q = np.random.uniform(robot.q_min, robot.q_max)
      i=0
      while True:
          pin.framesForwardKinematics(robot.model,data_ik,q)
          dMi = oMdes.actInv(data_ik.oMf[robot.tip_id])
          err = pin.log(dMi).vector
          if np.linalg.norm(err) < eps:
              success = True
              break
          if i >= IT_MAX:
              success = False
              break
          J = pin.computeFrameJacobian(robot.model,data_ik,q,robot.tip_id)
          v = - J.T.dot(np.linalg.solve(J.dot(J.T) + damp * np.eye(6), err))
          q = pin.integrate(robot.model,q,v*DT)
          if qlim == True:
              q = np.clip(q, robot.q_min+margin, robot.q_max-margin)
          if not i % 10 and verbose:
              print('%d: error = %s' % (i, err.T))
          i += 1

      if verbose:
          if success:
              print("Convergence achieved!")
              break  
          else: # if convergence is not achieved and no initial guess is provided, try again with a random guess
              print(f"\nWarning: the iterative algorithm has not reached convergence to the desired precision, for the try number {t}/{tries}")

  return np.array(q)

# %% build robot

path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
panda = RobotWrapper("panda_link8", os.path.join(path, "src/franka_ros/franka_description/panda.urdf"),
                     mesh_path=os.path.join(path, "src/franka_ros/franka_description"))
# panda_pykin = SingleArm("urdf/panda/panda_corrected.urdf")
# panda_pykin.setup_link_name(base_name="world", eef_name="panda_link8")

# %% build pose at vertices

vertices = random.sample(ISOCUBE_VERTICES.tolist(), 8)
vertices_shift = np.concatenate((vertices[-1:], vertices[:-1]))
direction_vectors = vertices - vertices_shift

dir_z_norm = direction_vectors/np.linalg.norm(direction_vectors)

# compute two perpendicular directions
dir_x = np.array([[-norm_dir[1], norm_dir[0], 0.0] if np.linalg.norm([-norm_dir[1], norm_dir[0], 0.0]) >= 1e-6 
               else [0.0, -norm_dir[2], norm_dir[1]] for norm_dir in dir_z_norm])
dir_x_norm = np.array([dir_xi/np.linalg.norm(dir_xi) for dir_xi in dir_x])
dir_y = np.array([np.cross(norm_dir, dir_xi) for norm_dir, dir_xi in zip(dir_z_norm, dir_x_norm)])
# stack all directional vectors to build a rotation matrix
rot_matrices = np.array([np.column_stack((v1,v2,v3)) for v1,v2,v3 in zip(dir_x_norm, dir_y, dir_z_norm)])
# build se3 from rot matrices and vertices
se3_list = [SE3(np.array(rot_matrix), np.array(vertex)) for rot_matrix, vertex in zip(rot_matrices, vertices)]

# %% cube

box = meshcat.geometry.Box(lengths=[LENGTH,LENGTH,LENGTH])
mat = meshcat.geometry.MeshBasicMaterial(color=0x0022ff, wireframe=True, linewidth=3, opacity=0.2)
isobox = meshcat.geometry.Box(lengths=[REAL_LENGTH,REAL_LENGTH,REAL_LENGTH])
iso_mat = meshcat.geometry.MeshBasicMaterial(color=0xf08080, opacity=0.5)
offset = SE3(np.eye(3), np.array([REAL_LENGTH/2, 0, REAL_LENGTH/2]) + np.array(OFFSETS))

# %%
panda.open_viewer()
# panda.add_geometry(object=ell, name_id="ell")
panda.add_geometry(object=box, name_id="cube", material=mat, transform=offset)
panda.add_geometry(object=isobox, name_id="isocube", material=iso_mat, transform=SE3(np.eye(3), np.array([REAL_LENGTH/2, 0, REAL_LENGTH/2])))

q_mean = (panda.model.upperPositionLimit.T + panda.model.lowerPositionLimit.T)/2
panda.update_visualisation(q=q_mean)

time.sleep(2)

q_des_list = []
q_des_list_2 = []
for se3 in se3_list:
  q_des_list_2.append(ik_with_margin(panda, se3, verbose=False, q=q_mean, qlim=True, margin=0.0, tries=10))
  q_des_list.append(panda.ik(se3, verbose=True, q=q_mean, qlim=True, tries=10))

try:
  while True:
    for q_des in q_des_list:
        panda.update_visualisation(q=q_des)
        time.sleep(3)
    panda.update_visualisation(q=q_mean)
    time.sleep(3)
except KeyboardInterrupt:
   pass

# %% Check joint limits

for q in q_des_list:
  min_reached = np.array(q) <= panda.q_min 
  max_reached = np.array(q) >= panda.q_max 

# %%

# %% Saved pose
# config1 = np.array([ 0.14028606,  0.64467816,  0.17581362, -1.94249138, -0.19584790, 2.57098698, -1.12402598])
# config2 = np.array([-0.11747514,  0.30399197, -0.25546747, -2.75065952, -1.14171283, 1.59995921,  1.45191951])
# config3 = np.array([-0.58874027,  0.07951421,  0.24171357, -2.02337672, -3.40536979, 1.79140052,  1.86724067])
# config4 = np.array([ 0.00485702, -0.7963841 ,  0.17954417, -2.67979661,  1.44971328, 0.91537867,  2.7361665 ])
# config5 = np.array([ 0.21972038,  0.07568444,  0.05876182, -1.80814478, -2.39510529, 2.72507742,  0.86407333])

configs = [[-2.38759312, -1.33166268,  1.73203202, -0.90198799,  1.28425421, 4.68780174,  1.70290726],
           [-0.37332436,  0.0738422 , -0.2418471 , -2.87396288, -1.26434147, 2.1569517 ,  1.33479013],
           [-0.58874027,  0.07951421,  0.24171357, -2.02337672, -3.40536979, 1.79140052,  1.86724067],
           [-0.19472872,  0.63909719, -0.12177556, -1.94492991,  0.13568283, 2.57630605, -1.97821508],
           [ 0.50192854,  0.79081094, -0.39624177, -1.85702397,  1.89983144, 1.44877087, -4.12775641]]

panda.open_viewer()
# panda.add_geometry(object=ell, name_id="ell")
panda.add_geometry(object=box, name_id="cube", material=mat, transform=offset)

q_mean = (panda.model.upperPositionLimit.T + panda.model.lowerPositionLimit.T)/2
panda.update_visualisation(q=q_mean)

time.sleep(2)

try:
  while True:
    for q_des in configs:
        panda.update_visualisation(q=np.array(q_des))
        time.sleep(3)
except KeyboardInterrupt:
   pass

# # %%
# idx = 0
# print(se3_list[idx])
# q_des = panda.ik(se3_list[idx], verbose=False, q=q_mean)
# panda.update_visualisation(q=q_des)

# # %%

# r0 = Rotation.from_matrix(se3_list[idx].rotation)
# print(r0.as_matrix())
# SE3r0 = SE3(r0.as_matrix(), np.array(se3_list[idx].translation))
# q_des = panda.ik(SE3r0, verbose=False, q=q_mean)
# panda.update_visualisation(q=q_des)

# %%
