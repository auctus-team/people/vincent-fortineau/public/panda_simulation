# %% IMPORTS 
# %matplotlib qt

import numpy as np
import time
from copy import deepcopy
from matplotlib import pyplot as plt
from tqdm import tqdm # for progression bar

import pinocchio as pin

from panda_sim.utils.robot_solver import RobotSolver
from panda_sim.sim_vs_exp import RobotState, interpolate_data, extract_data
from panda_sim.utils.task_ctrl import TaskController
from panda_sim.simulation import RK4, robot_state_ode, computeCmd, computeTorqueQPCmd
from panda_sim.utils.torque_qp import TorqueQP

# %% MACROS
# simulation
BASE = 'world'
TIP_LINK = 'panda_link8'
LOOP_FREQUENCY = 1e3 # Hz
# MAIN TASK GAINS
KP = np.array([1000,1000,1000,63,63,63])
KD = 2*np.sqrt(KP)
# SECONDARY TASK GAINS
KQP = np.array([100,100,100,100,100,100,100])
KQD = 2*np.sqrt(KQP)
# REGULARISATION TASK GAINS
KP_REG = 10*np.ones(7)
W_REG = 1e-5
# FV,FC&FO FRICTION TORQUE COEFFICIENTS (values taken from Gaz et al (2019) supplementary data)
FV = np.array([0.0665, 0.1987, 0.0399, 0.2257, 0.1023, -0.0132, 0.0638])
FC = np.array([0.2450, 0.1523, 0.1827, 0.3591, 0.2669, 0.1658, 0.2109])
FO = np.array([-0.1073, -0.1566, -0.0686, -0.2522, 0.0045, 0.0910, -0.0127])
# experimental
# should be in: panda_sim/data/ 
FILE_LIST = ['test_10N_x', 'test_10N_y', 'test_10N_z']
FILE_LIST_FOLDER = 'config_1/'
FILE_LIST_EXTENSION = '.csv'
FILE_LIST = [FILE_LIST_FOLDER + file_name + FILE_LIST_EXTENSION for file_name in FILE_LIST]
ARM_ID = 'panda_1'
NB_JOINTS = 7
STEP = 0.001 # 1 ms

# %% FUNCTIONS
#
def read_raw_csv(file_name:str):
  file_path = "../data/" + file_name
  return np.genfromtxt(file_path, delimiter=',', names=True, deletechars='')

def plot_endpoint_position(exp_data, sim_data, sim_data_2, title=None):
  exp_x = [pin.SE3(robot.forward(q)) for q in exp_data.q.T]
  sim_x = [pin.SE3(robot.forward(q)) for q in sim_data.q.T]
  sim2_x = [pin.SE3(robot.forward(q)) for q in sim_data_2.q.T]

  exp_position = np.array([pose.translation for pose in exp_x])
  sim_position = np.array([pose.translation for pose in sim_x])
  sim2_position = np.array([pose.translation for pose in sim2_x])

  axis = ['X', 'Y', 'Z']

  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    axs[it].plot(exp_data.t, exp_position[:,it], linewidth=2, color='r', alpha=0.8, label='Experimental')
    axs[it].plot(exp_data.t, sim_position[:,it], linewidth=1.5, color='b', alpha=0.8, label='Simulation')
    axs[it].plot(exp_data.t, sim2_position[:,it], linewidth=1.5, color='g', alpha=0.8, label='Simu QP')
    axs[it].title.set_text(axis[it] + ' axis')
    if any(exp_data.fext[it,:]):
      ax_f = axs[it].twinx()
      ax_f.plot(exp_data.t, exp_data.fext[it,:], linewidth=1.5, color='k', linestyle='dashed', alpha=0.8, label='Force Reaction')
  axs[-1].legend()
  ax_f.legend()
  if title is None:
    fig.suptitle('Endpoint position')
  else:
    fig.suptitle(title)

def delete_string_sequences(main_string, string_sequences):
  for sequence in string_sequences:
    main_string = main_string.replace(sequence, '')
  return main_string

#######################
# %% MAIN
if __name__ == "__main__":
  robot = RobotSolver(BASE, TIP_LINK, is_visualisation=False)
  robot.set_coulomb_friction_gains(FC, FO)
  robot.set_viscous_friction_gain(FV)

  robot_qp = deepcopy(robot)

  # set up controllers gains and targets
  main_task = TaskController(ctrl_mode='endpoint', damping_gain=KD, stiffness_gain=KP)
  main_task_qp = deepcopy(main_task)
  secondary_task = TaskController(ctrl_mode='joint', nb_joint_ctrl=robot.model.nq, damping_gain=KQP, stiffness_gain=KQD)
  trq_qp = TorqueQP(robot, dt=STEP, k_reg=KP_REG, rw=W_REG, solver='qpoases')

  # %% Multiple files case
  exp_data_list = []
  sim_data_list = []
  sim_data_qp_list = []

  t_idx = 0
  joint_pose_idx = list(range(1, NB_JOINTS+1))
  joint_eff_idx = list(range(NB_JOINTS+1, 2*NB_JOINTS+1))
  wrench_idx = list(range(2*NB_JOINTS+1, 2*NB_JOINTS+6+1)) # 6 DOF

  for file_idx, file_name in enumerate(FILE_LIST):
    #############################
    # collect experimental data
    #############################
    print("Reading file: {}...\n".format(file_name))
    data, idxs = extract_data(read_raw_csv(file_name), ARM_ID, NB_JOINTS, set_t0=True)
    data = interpolate_data(data, 0, step=STEP) # interpolation at 1ms
    exp_data = RobotState(NB_JOINTS, t=data[t_idx], q=data[joint_pose_idx], tau=data[joint_eff_idx], fext=data[wrench_idx])
    del data

    #############################
    # simulate the experimental data
    #############################
    robot.update_joint_data(q=exp_data.q[:,0]) # set intial joint config
    robot_qp.update_joint_data(q=exp_data.q[:,0]) 
    #
    main_task.setEndpointTarget(x_star=pin.SE3(robot.forward())) # update marker to current pose
    main_task_qp.setEndpointTarget(x_star=pin.SE3(robot.forward())) # update marker to current pose
    secondary_task.setJointTarget(q_star=robot.q) # update to current joint position
    #
    sim_data = RobotState(nb_joints=7)  
    sim_data_qp = RobotState(nb_joints=7) 
    tau = exp_data.tau[:,0]
    tau_qp = exp_data.tau[:,0]
    # 
    start_time = time.time()
    for t in tqdm(exp_data.t, desc="Simulation n{} in progress..".format(file_idx), miniters=10):
      idx = np.where(exp_data.t==t)[0].squeeze()
      #
      sim_data.add_joint_states(t, q=robot.q, dq=robot.dq, tau=tau, fext=exp_data.fext[:,idx])
      sim_data_qp.add_joint_states(t, q=robot_qp.q, dq=robot_qp.dq, tau=tau_qp, fext=exp_data.fext[:,idx])
      # compensation model control
      tau = computeCmd(robot, main_task, secondary_task, is_saturation=False)
      dstate = RK4(robot_state_ode, np.concatenate((robot.q, robot.dq)), np.concatenate((tau, exp_data.fext[:,idx])), STEP, arg=robot)
      robot.update_joint_data(dstate[0:robot.model.nq], dstate[robot.model.nq:])
      # torque qp control
      tau_qp = computeTorqueQPCmd(robot_qp, main_task, trq_qp)
      dstate_qp = RK4(robot_state_ode, np.concatenate((robot_qp.q, robot_qp.dq)), np.concatenate((tau_qp, exp_data.fext[:,idx])), STEP, arg=robot_qp)
      robot_qp.update_joint_data(dstate_qp[0:robot_qp.model.nq], dstate_qp[robot_qp.model.nq:])

    print("Duration of the simulation n{}: {:.1f}s".format(file_idx, time.time() - start_time))  
    
    # 
    exp_data_list.append(exp_data)
    sim_data_list.append(sim_data)
    sim_data_qp_list.append(sim_data_qp)

    plot_endpoint_position(exp_data, sim_data, sim_data_qp, title=delete_string_sequences(file_name, [FILE_LIST_FOLDER, FILE_LIST_EXTENSION]))
    plt.pause(2)

    del exp_data, sim_data
# %%
