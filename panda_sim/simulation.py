# %% IMPORTS 
# %matplotlib ipympl
# %matplotlib qt

import os
import sys
import copy
from pathlib import Path

import numpy as np
import scipy.signal as sig
import time
import math

import pinocchio as pin

from typing import Tuple

from matplotlib import pyplot as plt
from matplotlib import ticker as tck

from panda_sim.utils.robot_solver import RobotSolver
from panda_sim.utils.task_ctrl import TaskController as tc
from panda_sim.utils.static_error_solver import RobotStaticErrorSolver as rses
from panda_sim.utils.torque_qp import TorqueQP

# %% MACROS
CSV_FILE_NAME = 'panda_sim_01' # without extension
BASE = 'world'
TIP_LINK = 'panda_link8'
IS_REAL_TIME = False # real time is not yet supported, set to False
IS_VISUALISATION = True # 3D visualisation with meshcat
IS_SATURATION = False # joint saturation of the robot
SAVE_DATA = False
PLOT_DATA = True
DISPLAY_ERROR_POLYTOPES = False # in the 3D visualisation
LOOP_FREQUENCY = 1e3 # Hz
VISUAL_FREQUENCY_UPDATE = 25 # Hz
BUFFER_SIZE = 120*LOOP_FREQUENCY # 120s of data
T_MAX = 8 # max duration of the simulation
INITIAL_CONFIG = np.array([np.pi/2,-0.1871,0,-2.0942,0,1.90707,0])
# CONTROL METHOD
TORQUE_QP = 1
MODEL_COMP = 2
CTRL_METHOD = TORQUE_QP
# MAIN TASK GAINS (endpoint stiffness and damping ctrl)
KP = np.array([1000,1000,1000,63,63,63]) 
KD = 2*np.sqrt(KP) 
# SECONDARY TASK GAINS
IS_SECONDARY_TASK = False 
KQP = 100*np.ones(7)
KQD = 2*np.sqrt(KQP)
# REGULARIsATION TASK GAINS
KP_qp = 10*np.ones(7) # for velocity proportionnal gain
RW = 1e-5 # regularisation weight

# GLOBAL VARIABLE
# data vector
t_vec = []
q_vec = []
dq_vec = []
tau_vec = []
x_vec = []
fext_vec = []

# %% FUNCTIONS
# functions

def force_pert_seq(t):
  if ((t > 3) and (t < 7) ):
    return np.array([0.,0.,10.,0.,0.,0.]) # 10N on z
  elif ( (t > 10) and (t < 14) ):
    return -1*np.array([0.,0.,10.,0.,0.,0.]) # -10N on z
  elif ( (t > 17) and (t < 21) ):
    return np.array([0.,10.,0.,0.,0.,0.]) # 10N on y
  elif ( (t > 24) and (t < 28) ):
    return np.array([0.,-10.,0.,0.,0.,0.]) # -10N on y
  elif ( (t > 31) and (t < 35) ):
    return np.array([10.,0.,0.,0.,0.,0.]) # 10N on x
  elif ( (t > 38) and (t < 42) ):
    return np.array([-10.,0.,0.,0.,0.,0.]) # -10N on x
  else:
    return np.zeros(6)

def plot():
  axes = ['x', 'y', 'z']

  t_vec_plot = np.array(t_vec)
  t_vec_plot.shape = (t_vec_plot.shape[0],1)
  q_vec_plot = np.array(q_vec)
  dq_vec_plot = np.array(dq_vec)
  tau_vec_plot = np.array(tau_vec)
  fext_vec_plot = np.array(fext_vec)

  # position_vec_plot = []
  # quaternion_vec_plot = []
  # # convert se3 position to translation and quaternion
  # for elem in x_vec:
  #   position_vec_plot.append(elem.translation)
  #   tmp = pin.Quaternion(elem.rotation)
  #   quaternion_vec_plot.append([tmp.x, tmp.y, tmp.z, tmp.w])

  # position_vec_plot = np.array(position_vec_plot)
  # quaternion_vec_plot = np.array(quaternion_vec_plot)
  simu_error_vec = []
  for x in x_vec:
    adj = np.concatenate( (np.concatenate( (x.rotation, np.zeros((3,3))), axis=1 ),np.concatenate( (np.zeros((3,3)), x.rotation), axis=1 )), axis=0 )
    simu_error_vec.append(adj.dot(pin.log6(x.actInv(x_vec[0])).vector))
  simu_error_vec = np.array(simu_error_vec)

  static_errors = rses(RobotSolver(BASE, TIP_LINK), "endpoint_impedance")
  static_error_vec = []

  for qi, fi in zip(q_vec_plot, fext_vec_plot):
    static_error_vec.append(static_errors.computeSE(qi, np.diag(KP), fi))
  static_error_vec = np.array(static_error_vec)

  dynamic_error_vec = np.array(static_errors.computeDynamicErrorTVSS(np.diag(KP), np.diag(KD), q_vec_plot, f_in_func=force_pert_seq))

  m2cm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*100))
  fig, axs = plt.subplots(2,3)
  for it in range(0,3):
    axs[0,it].plot(t_vec_plot, fext_vec_plot[:,it], linewidth=0.8, color='r', alpha=0.8)
    axs[0,it].set_title(axes[it] + ' force (N)')
    axs[1,it].plot(t_vec_plot, simu_error_vec[:,it], linewidth=0.8, color='r', alpha=0.8)
    axs[1,it].plot(t_vec_plot, static_error_vec[:,it], linewidth=0.8, color='g', linestyle='dashed', alpha=0.8)
    axs[1,it].plot(t_vec_plot, dynamic_error_vec[:,it], linewidth=0.8, color='b', linestyle='dotted', alpha=0.8)
    axs[1,it].set_title(axes[it] + ' error position (cm)')
    axs[1,it].yaxis.set_major_formatter(m2cm)
  fig.suptitle('Interaction forces & motion errors generated $(\epsilon=x_0-x)$')

  filter = sig.butter(N=2, Wn=0.5/ (LOOP_FREQUENCY/2), btype='lowpass', output='sos') # normalized cutoff freq (fc/(fs/2))
  estimation_error = simu_error_vec[:,0:3] - static_error_vec[:,0:3]
  estimation_error_filt = np.array([sig.sosfiltfilt(filter, estimation_error_sig) for estimation_error_sig in estimation_error.T]).T

  # the signal is filtered to avoid peaks due to unmodeled transitional behaviour 
  m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
  fig, axs = plt.subplots(1,3)
  for it in range(0,3):
    # axs[it].plot(t_vec_plot, position_vec_plot[0,it]-position_vec_plot[:,it] - static_error_vec[:,it], linewidth=0.8, color='r', alpha=0.4)
    axs[it].plot(t_vec_plot, estimation_error_filt[:,it], linewidth=0.8, color='r', alpha=0.8)
    #with autoscale_turned_off():
    lims = [axs[it].get_xlim(), axs[it].get_ylim()]
    axs[it].set_xlim(lims[0])
    axs[it].set_ylim(lims[1])
    axs[it].plot(t_vec_plot, estimation_error[:,it], linewidth=0.8, color='r', alpha=0.4)
    axs[it].set_title(axes[it] + ' error (mm)')
    axs[it].yaxis.set_major_formatter(m2mm)
  fig.suptitle('Estimation error, between simulated position (r) and estimated deviation $(\epsilon)$')

  fig, axs = plt.subplots(2,4)
  for it in range(0, tau_vec_plot.shape[1]):
    axs[math.floor(it/4), it%4].plot(t_vec_plot, tau_vec_plot[:,it], linewidth=0.8, color='r')
    axs[math.floor(it/4), it%4].set_title('Joint ' + str(it+1) + ' effort (Nm)')
  fig.suptitle('$\tau$')

# Save data to a csv file
def save():

  t_vec_save = np.array(t_vec)
  t_vec_save.shape = (t_vec_save.shape[0],1)
  q_vec_save = np.array(q_vec)
  dq_vec_save = np.array(dq_vec)
  tau_vec_save = np.array(tau_vec)
  fext_vec_save = np.array(fext_vec)

  axes = ['x', 'y', 'z']
  quat_axes = ['x', 'y', 'z', 'w']

  header_list = "t,"
  for i in range(0,np.array(q_vec_save).shape[1]):
    header_list = header_list + 'joint' + str(i+1) + '_position,'
  for i in range(0,np.array(dq_vec_save).shape[1]):
    header_list = header_list + 'joint' + str(i+1) + '_velocity,' 
  for i in range(0,np.array(tau_vec_save).shape[1]):
    header_list = header_list + 'joint' + str(i+1) + '_effort_c,'
  for i in range(0,3):
    header_list = header_list + 'endpoint_position_' + axes[i] + ','
  for i in range(0,4):
    header_list = header_list + 'endpoint_orientation_' + quat_axes[i] + ','
  for i in range(0,3):
    header_list = header_list + 'endpoint_force_' + axes[i] + ','
  for i in range(0,3):
    header_list = header_list + 'endpoint_torque_' + axes[i] + ','
  
  position_vec = []
  quaternion_vec = []
  # convert se3 position to translation and quaternion
  for elem in x_vec:
    position_vec.append(elem.translation)
    tmp = pin.Quaternion(elem.rotation)
    quaternion_vec.append([tmp.x, tmp.y, tmp.z, tmp.w])

  position_vec = np.array(position_vec)
  quaternion_vec = np.array(quaternion_vec)

  path = os.path.dirname(os.path.abspath(''))
  csv_full_name = os.path.join(path, CSV_FILE_NAME + '.csv')

  np.savetxt(csv_full_name, np.concatenate((t_vec_save, q_vec_save, dq_vec_save, tau_vec_save, position_vec, quaternion_vec, fext_vec_save), axis=1), delimiter=',', header=header_list)
  print("Data saved.")

# conversion from rotation matrix to euler angles, /!\ Euleur angles should be avoided
def fromSO3toEulerAngle(rot_matrix:np.ndarray['np.ndarray']) -> np.ndarray:
  return np.array([math.atan2(rot_matrix[1,0],rot_matrix[0,0]), 
                  math.atan2(-rot_matrix[2,0],np.sqrt(rot_matrix[2,1]**2+rot_matrix[2,2]**2)),
                  math.atan2(rot_matrix[2,1],rot_matrix[2,2])])

# TODO: 
def looping_rate(dt_des:float, t_old:float) -> Tuple[float, float]:
  '''
  Try to insure a loop duration to the desired rate. If the loop duration is shorter than the desired rate, then the function sleep for the additional time. Otherwise, the function returns the actual rate.
  Args:
  - dt_des:   desired loop duration
  - t_old:    starting time of the current/previous loop

  Return:
  - real_dt:  actual loop duration achieved
  - t_new:    new time for the next loop
  '''
  t_new = time.time()
  dt_loop = t_old - t_new
  diff_dt = dt_des - dt_loop
  if(diff_dt > 0):
    real_dt = dt_des
    time.sleep(diff_dt)
  else:
    real_dt = dt_loop
  return real_dt, t_new

# 4th order Runge Kutta integration
def RK4(func, x, u, dt, arg=None):
  '''
  4th Runge Kutta integration
  Args:
  - func:   ode function dx = x
  - x:      state at time k
  - u:      input
  - dt:     sampling time (dt = t[k+1]-t[k])
  - arg:    possible necessary parameters, or arguments for the ode function

  Return:
  - xn:     state at time k+1
  '''
  if arg is None:
    k1 = func(x, u)
    k2 = func(x+0.5*dt*k1, u)
    k3 = func(x+0.5*dt*k2, u)
    k4 = func(x+dt*k3, u)
  else:
    k1 = func(x, u, arg)
    k2 = func(x+0.5*dt*k1, u, arg)
    k3 = func(x+0.5*dt*k2, u, arg)
    k4 = func(x+dt*k3, u, arg)
  #
  return x + dt/6 * (k1 + 2*k2 + 2*k3 + k4)

def robot_state_ode(x, u, robot_model):
  n = robot_model.model.nq
  q = x[0:n]
  dq = x[n:]
  tau = u[0:n]
  fext = u[n:]
  #
  dx1 = dq 
  dx2 = robot_model.direct_dynamic(q, dq, tau, fext) # ddq
  return np.concatenate((dx1, dx2))

def writeMeshFileObj(self, vertices_list, faces_index_list, file_name, center=None, file_path=None):
  if file_path is None:
    mod_path = Path(__file__).parent
    rel_path = ".utils/meshes/visual"
    file_path = str((mod_path / rel_path).resolve())
  elif file_path[-1] == "/":
    file_path = file_path[:-1] # delete last char

  # center the polytope over a 3d point (eg. the robot endpoint)
  if center is not None:
    center = np.array(center).reshape(1,-1)
    vertices_list = vertices_list + center
  
  file = open(file_path + "/" + file_name, 'w') # create 
  for vert in vertices_list:
    file.write("v {0} {1} {2}\n".format(vert[0], vert[1], vert[2]))
  for face_idx in faces_index_list:
    file.write("f {0} {1} {2}\n".format(*[i+1 for i in face_idx]))
  file.close()
  return file_path + "/" + file_name

def computeCmd(robot:RobotSolver, main_task:tc, secondary_task:(tc or None or str)=None, is_saturation=False):
  # robot main task, desired endpoint acceleration
  ddx_d = main_task.computeEndpointImpedanceCmd(pin.SE3(robot.forward()), robot.jacobian().dot(robot.dq), reference_frame=pin.LOCAL_WORLD_ALIGNED)
  # conversion from endpoint to joint space with weighted pseudo inverse
  ddq_d = robot.jacobian_weighted_pseudo_inv(robot.q, robot.mass_matrix()).dot( ddx_d - robot.jacobian_dot().dot(robot.dq) )
  # torque cmd computation with dynamic compensation
  tau_d = robot.data.M.dot(ddq_d) + robot.coriolis_matrix().dot(robot.dq) + robot.gravity_torque(robot.q)
  # # secondary task 
  if secondary_task is None:
    tau_red = np.zeros(robot.model.nq)
  elif isinstance(secondary_task, tc):
    # (maintain initial config)
    if secondary_task.control_mode == 'joint':
      ddq_d2 = secondary_task.computeJointImpedanceCmd(robot.q, robot.dq)
      # secondary task torque projection in the robot torque null space
      tau_red = (np.eye(robot.model.nq) - robot.Jac.T.dot(robot.pWJac.T)).dot((robot.data.M.dot(ddq_d2)))
    # gravity compensation
    elif secondary_task.control_mode == 'gravity_torque':
      tau_d2 = secondary_task.computeJointTorqueGravityCompensationCmd(robot.gravity_torque(), robot.t)
      tau_red = (np.eye(robot.model.nq) - robot.Jac.T.dot(robot.pWJac.T)).dot(tau_d2)
  # saturate joint effort cmd
  if is_saturation:
    tau = robot.apply_joint_effort_limits(tau_d + tau_red)
  else:
    tau = tau_d + tau_red
  return tau

def computeTorqueQPCmd(robot:RobotSolver, main_task:tc, qpWrapper:TorqueQP, is_saturation=False):
  # robot main task, desired endpoint acceleration
  ddx_d = main_task.computeEndpointImpedanceCmd(pin.SE3(robot.forward()), robot.jacobian().dot(robot.dq), reference_frame=pin.LOCAL_WORLD_ALIGNED)
  # qp solver 
  tau = qpWrapper.update(robot.q, robot.dq, ddx_d)
  # saturate joint effort cmd
  if is_saturation:
    tau = robot.apply_joint_effort_limits(tau)
  return tau

def main(dt:float):
  # INIT
  robot = RobotSolver(BASE, TIP_LINK, q=INITIAL_CONFIG, is_visualisation=IS_VISUALISATION)
  # endpoint kinematics
  x = pin.SE3(robot.forward())

  f_ext = np.zeros(6)
  f_mag = 10 # force cube side (N), for polytopic representation
  tau = np.zeros(robot.model.nq)

  marker_pose = copy.copy(x)

  # set up controllers gains and targets
  main_task = tc(ctrl_mode='endpoint')
  main_task.setEndpointTarget(x_star=marker_pose, dx_star=np.zeros(6), ddx_star=np.zeros(6))
  main_task.setGains(damping_gain=KD, stiffness_gain=KP)
  secondary_task = tc(ctrl_mode='joint',nb_joint_ctrl=robot.model.nq)
  secondary_task.setJointTarget(q_star=INITIAL_CONFIG, dq_star=np.zeros(robot.model.nq), ddq_star=np.zeros(robot.model.nq))
  secondary_task.setGains(damping_gain=KQP, stiffness_gain=KQD)
  #
  if CTRL_METHOD == TORQUE_QP:
    torque_qp = TorqueQP(robot, dt=1./LOOP_FREQUENCY, k_reg=KP_qp, rw=RW, solver='qpoases')

  if IS_REAL_TIME:
    t1 = time.time()
  else:
    t1 = 0
  if IS_VISUALISATION:
    visual_rate = 1./VISUAL_FREQUENCY_UPDATE
    robot.update_visualisation()
    t_next_visu = t1 + visual_rate
    if DISPLAY_ERROR_POLYTOPES:
      static_errors = rses(RobotSolver(BASE, TIP_LINK), "endpoint_impedance")

  # save data
  t_vec.append(t1)
  q_vec.append(robot.q)
  dq_vec.append(robot.dq)
  tau_vec.append(tau)
  x_vec.append(x)
  fext_vec.append(f_ext)

  # MAIN LOOP
  while(t1 <= T_MAX):
    # external force
    f_ext = force_pert_seq(t1)
    # loop duration
    if IS_REAL_TIME:
      real_dt, t1 = looping_rate(dt, t1)
    else:
      real_dt = 1./LOOP_FREQUENCY
      t1 = t1 + real_dt
    # robot command
    if CTRL_METHOD == MODEL_COMP:
      tau = computeCmd(robot, main_task, secondary_task if IS_SECONDARY_TASK else None, is_saturation=IS_SATURATION)
    elif CTRL_METHOD == TORQUE_QP:
      tau = computeTorqueQPCmd(robot, main_task, torque_qp, is_saturation=IS_SATURATION)
      if tau is None:
        raise # todo: qp solving failed.
    else:
      raise ValueError
    dstate = RK4(robot_state_ode, np.concatenate((robot.q, robot.dq)), np.concatenate((tau, f_ext)), real_dt, arg=robot)   
    robot.update_joint_data(dstate[0:robot.model.nq], dstate[robot.model.nq:], apply_saturation=IS_SATURATION)
    # update endpoint kinematics
    x = pin.SE3(robot.forward())

    if IS_VISUALISATION:
      if t_next_visu < t1:
        # polytope display
        robot.update_visualisation()
        #
        if DISPLAY_ERROR_POLYTOPES:
          v, f = static_errors.computeSEPolytopeMesh(robot.q, np.diag(KP), f_mag)
          file_path = writeMeshFileObj(v, f, "static_error_polytope.obj", center=x.translation)
          robot.add_visual_object(file_path, "static_error_polytope") # TODO fix
        #
        t_next_visu = t1 + visual_rate

    # save data
    t_vec.append(t1)
    q_vec.append(robot.q)
    dq_vec.append(robot.dq)
    tau_vec.append(tau)
    x_vec.append(x)
    fext_vec.append(f_ext)
    if len(t_vec) > BUFFER_SIZE:
      t_vec.pop(0)
      q_vec.pop(0)
      dq_vec.pop(0)
      tau_vec.pop(0)
      x_vec.pop(0)
      fext_vec.pop(0)
    
# %% MAIN
if __name__ == "__main__":
  main(dt=float("{:.3f}".format(1.0/LOOP_FREQUENCY)))
  if SAVE_DATA:
    save()
  if PLOT_DATA:
    plot()
  # try:
  #   main(dt=float("{:.3f}".format(1.0/LOOP_FREQUENCY)))
  # except KeyboardInterrupt:
  #   print('Keyboard interruption!')
  #   try:
  #     sys.exit(130)
  #   except SystemExit:
  #     os._exit(130)
  # except Exception as e:
  #   print(e)
  # finally:
  #   if SAVE_DATA:
  #     save()
  #   if PLOT_DATA:
  #     plot()

 # %%