# Introduction

This repository contains a Python code that simulates the dynamic behavior of a robotic arm, the panda arm, using the Pinocchio library and Meshcat for visualization. 
The simulation aims to reproduce the movement of a robotic arm subjected to various external forces.

The Pinocchio library is a fast and efficient C++ implementation of the rigid body dynamics algorithms. 
The library provides analytical tools for the computation of the kinematics and dynamics of serial and parallel robots, and it has a Python interface. 
On the other hand, Meshcat is a web-based 3D viewer for visualizing robotics and other spatial systems. 
Meshcat has Python bindings that enable the creation of a browser-based visualization system for the simulation.

The main file of the simulation is `panda_sim.py`. 
The class `RobotSolver` is a wrapper of the Pinocchio library for a panda arm, the class `TaskController` allows the implementation of controllers for given trajectories (using SE3 representation for the 6D position), and the class `RobotStaticErrorSolver` provides some estimation of displacement boundaries when the robot is subject to bounded forces. 
This last class is detailed in the part Estimated errors.

# Prerequisites
To run the simulation, the following software must be installed on your machine:

- Python 3.9 or later
- pinocchio
- meshcat
- pycapacity
- numpy, scipy, matplotlib

# Installation [TODO]
1. Clone the repository to your local machine:
``` bash
git clone https://gitlab.inria.fr/auctus-team/people/vincent-fortineau/public/panda_simulation.git
```
2. Install the required Python packages:
```
conda env create -f=env.yaml
conda activate panda_sim_env
```

# Usage
To run the simulation, execute the `panda_sim.py` file in a Python environment. 
The simulation is parametrized using the variables in the Macros section of the code. 
You can modify these variables according to your needs to simulate the robotic arm under different conditions.

The `force_pert_seq()` function generates the external force sequence that acts on the robotic arm. 
You can modify this function to change the sequence of external forces applied to the arm.

The `RK4()` function implements the 4th order Runge Kutta integration method.

The `robot_state_ode` function provide the differential system for the robot state $\theta=(q, \dot{q})^t$ and the input $u=(\tau_d, f_{ext})^t$, with $q$ the joint state, $\tau_d$ the torque command and $f_{ext}$ the external forces applied on the robot, such as:

$$\ddot{q}=A^{-1}(q)\left( \tau_d - J(q)^t f_{ext} - C(q,\dot{q})\dot{q} - g(q) \right)$$

The `writeMeshFileObj` function generates a mesh file (.obj) according to the vertices and faces indexes of a 3D shape. 
By default, the genetated file is saved in the subfolder `utils/meshes/visual`.

The `looping_rate()` function (<u>not tested</u>) ensures a loop duration to the desired rate. 
If the loop duration is shorter than the desired rate, then the function sleeps for the additional time. 
Otherwise, the function returns the actual rate. 

The main function is detailed in the next section.

# Main loop
The main function initializes a `RobotSolver` object with the specified base and tip link, and then initializes the robot's configuration and velocity based on the specified `INITIAL_CONFIG` and null velocities. 
The robot wrapper uses URDF files and meshes, that sould be stored in `utils`. 
They are available [here](link_to_urdf). 
It also initializes a marker pose, that will be the target position for the controllers, using the forward kinematics of the initial configuration.

The function then creates two tasks: `main_task` and `secondary_task`. `main_task` is the primary task of the robot, which controls the robot's endpoint position and velocity using an impedance controller with the gains `KP` and `KD`. 

$$\ddot{x}_d=\ddot{x}^* + K_D (\dot{x}^* - \dot{x}) + K_P (x^* - x)$$

In this program, the position $x$ is defined as an SE3 object. 
The computation of the positional error is based on Rodrigues' formula (cf. 3.2.3.3 Matrix Logarithm of Rotations, in Modern Robotics Mechanics, Planning, and Control):
$$H_{adj} (log_6(SE3(x) SE3(x^*)^{-1}))$$
The adjoint matrix $H_{adj}$ form depends on the reference frame used, to convert the error to the local frame.

The endpoint acceleration $\ddot{x}_d$ is then converted to the joint space using the jacobian mapping:
$$\ddot{q}_d = J^{A+} \left( \ddot{x}_d - \dot{J}(q)\dot{q} \right)$$
The inverse jacobian is approximated with pseudo inverse the weighted with the inertia matrix: $J(q)^{A+} = A(q)^{-1} J(q)^t \left(J(q)A(q)^{-1}J(q)^t\right)^{-1}$.
The torque command is then computed to compensate the dynamic model of the robot:
$$\tau_d=A(q)\ddot{q}_d + C(q,\dot{q})\dot{q} + g(q)$$

`secondary_task` is an optional, the task is projected in the null space of the robot jacobian to manage the redundancy of the robot. 
It maintains the robot's joint configuration at the initial configuration using a joint impedance controller with gains `KQP` and `KQD`. 

$$\tau_{red}=( I - J(q)^tJ(q)^{A+})A(q)\ddot{q}_{d2}$$

The main loop then iterates until the `T_MAX` time is reached. 
Within each iteration, it updates the robot's command and then applies the torques to the robot's joints and simulates the robot's dynamic motion using a 4th order Runge-Kutta integrator, with the robot differential system `robot_state_ode`. 

If `IS_SATURATION` is True, the resulting joint positions and velocities are saturated to their respective limits. 
Finally, the robot's endpoint kinematics and velocity are updated using the forward kinematics and Jacobian of the robot.

# Estimated errors
The class `RobotStaticErrorSolver` provide polytopic boundaries of static errors in the presence of bounded forces input. The static error polytope can be computed considering the endpoint dynamic model of the robot:

$$\ddot{x} = J(q)\ddot{q}_d + \dot{J}(q)\dot{q} - J(q)A(q)^{-1}J(q)^{t} f_{ext}$$

The desired joint acceleration $\ddot{q}_d$ can be expressed at the endoint level with the jacobian mapping $\dot{q}_d=J(q)^{+} \left( \ddot{x}_d - \dot{J}(q)\dot{q} \right)$:

$$\ddot{x} = J(q)J(q)^{+} \left( \ddot{x}_d - \dot{J}(q)\dot{q} \right) + \dot{J}(q)\dot{q} - J(q)A(q)^{-1}(q)J(q)^t f_{ext}$$

The endpoint desired acceleration $\ddot{x}_d$ is remplaced by the main task controller:

$$\ddot{x} = \ddot{x}^* + K_D (\dot{x}^* - \dot{x}) + K_P (x^* - x) - J(q)A(q)^{-1}(q)J(q)^t f_{ext}$$

The error $\epsilon=x^* - x$, can be used to write the dynamic behavior of the error:

$$\ddot{\epsilon} + K_D \dot{\epsilon} + K_P \epsilon = J(q)A(q)^{-1}(q)J(q)^t f_{ext}$$

The static gain of this 2nd order model at the neighbourhood of a configuration $q_0$ is given by the formula, with $f_0$ the input external force:

$$\lim_{t->\infty}\epsilon(t)=K_P^{-1}J(q_0)A(q_0)^{-1}J(q_0)^t f_0$$

# License
This code is licensed under the [Creative Commons Attribution 4.0 International (CC BY 4.0) license](https://creativecommons.org/licenses/by/4.0/). 
You are free to share and adapt this code for any purpose, even commercially, as long as you give appropriate credit and indicate if changes were made. 
For more details, please see the [full license text](https://creativecommons.org/licenses/by/4.0/legalcode).

# Acknowledgments
This simulation was developed by Vincent Fortineau as part of the Litchie project. The simulation code was inspired by the examples provided in the Pinocchio library documentation and the Meshcat website. It uses work developped by Antun Skuric on polytopic computation.
